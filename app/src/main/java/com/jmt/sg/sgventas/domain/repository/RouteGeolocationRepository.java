package com.jmt.sg.sgventas.domain.repository;

import com.jmt.sg.sgventas.domain.interactor.RouteGeolocationInteractor;
import com.jmt.sg.sgventas.domain.model.LocationHistoryE;

import java.util.ArrayList;

/**
 * Created by jmtech on 5/12/16.
 */
public interface RouteGeolocationRepository {
    void storeGeolocation(double lat, double lng, final RouteGeolocationInteractor.Callback rCallback);
    void storeGeolocation(ArrayList<LocationHistoryE> positions, final RouteGeolocationInteractor.Callback rCallback);
}