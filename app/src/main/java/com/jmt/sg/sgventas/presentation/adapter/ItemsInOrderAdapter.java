package com.jmt.sg.sgventas.presentation.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.jmt.sg.sgventas.R;
import com.jmt.sg.sgventas.domain.model.ItemInOrderE;
import com.jmt.sg.sgventas.domain.model.ItemProductE;
import com.jmt.sg.sgventas.presentation.presenter.InputNumberDialogPresenter;
import com.jmt.sg.sgventas.presentation.view.InputNumberDialogView;

import java.util.ArrayList;
import java.util.Locale;

import io.realm.processor.Utils;

/**
 * Created by jmtech on 5/20/16.
 */
public class ItemsInOrderAdapter extends RecyclerView.Adapter<ItemsInOrderAdapter.ViewHolder> {
    private Context ctx;
    private ItemsOrderListener itemsListener;
    private ArrayList<ItemInOrderE> data;
    private boolean editable;
    public ItemsInOrderAdapter(Context _ctx, ArrayList<ItemInOrderE> data, boolean editable, ItemsOrderListener itemsListener){
        ctx = _ctx;
        this.data = data;
        this.editable = editable;
        this.itemsListener = itemsListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.lv_item_order_product, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final ItemInOrderE obj = data.get(position);

        holder.tv_nombre.setText(obj.getName());

        holder.tv_amount.setText(String.valueOf(obj.getQuantity()));

        String price = String.format(Locale.ENGLISH,"%.2f",obj.getPrice());
        holder.tv_price.setText(price);

        String total = String.format(Locale.ENGLISH,"%.2f",obj.getQuantity()*obj.getPrice());
        holder.tv_total.setText(total);

        if(editable){
            holder.tv_amount.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new InputNumberDialogPresenter(new InputNumberDialogView() {
                        @Override
                        public void clickConfirm(int cant) {
                            holder.tv_amount.setText(String.valueOf(cant));
                            itemsListener.onUpdateAmmount(position,cant);
                        }
                        @Override
                        public void clickCancel() {}
                        @Override
                        public Context getContext() {
                            return ctx;
                        }
                    }).showInputNumberDialog(obj.getQuantity());
                }
            });

            holder.btnRemoveItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemsListener.onRemoveItemClick(position);
                }
            });
        }else{
            holder.tv_amount.setEnabled(false);
            holder.btnRemoveItem.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tv_nombre,tv_price,tv_total;
        private EditText tv_amount;
        private AppCompatButton btnRemoveItem;
        public ViewHolder(View itemView) {
            super(itemView);
            tv_nombre = (TextView)itemView.findViewById(R.id.tv_name);
            tv_price = (TextView)itemView.findViewById(R.id.tv_price);
            tv_amount = (EditText)itemView.findViewById(R.id.txt_amount);
            tv_total = (TextView)itemView.findViewById(R.id.tv_total);
            btnRemoveItem = (AppCompatButton)itemView.findViewById(R.id.btnRemoveItem);
        }
    }

    public interface ItemsOrderListener{
        void onRemoveItemClick(int pos);
        void onUpdateAmmount(int pos, int ammount);
    }
}