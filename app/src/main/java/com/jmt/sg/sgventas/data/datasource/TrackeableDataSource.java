package com.jmt.sg.sgventas.data.datasource;

import com.jmt.sg.sgventas.domain.model.AgentTrackeable;
import com.jmt.sg.sgventas.domain.repository.RepositoryCallback;

/**
 * Created by jmtech on 5/13/16.
 */
public interface TrackeableDataSource {
    void saveAgent(AgentTrackeable agentTrackeable, RepositoryCallback repositoryCallback);

    void getAgent(RepositoryCallback repositoryCallback);

    void removeAgent(RepositoryCallback repositoryCallback);
}