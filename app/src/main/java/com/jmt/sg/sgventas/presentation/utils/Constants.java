package com.jmt.sg.sgventas.presentation.utils;

/**
 * Created by jmtech on 5/26/16.
 */
public final class Constants {
    public static final class VISIT_STATE{
        public static final String VisitState = "VISIT_STATE";
        public static final int Scheduled = 1;
        public static final int Done = 2;
        public static final int ReScheduling = 3;
        public static final int Cancelled = 4;
    }
    public static final class ORDER_STATE{
        public static final String OrderState = "ORDER_STATE";
        public static final int New = 0;
        public static final int Pending = 1;
        public static final int Confirmed = 2;
        public static final int Done = 3;
        public static final int Anulled = 4;
    }
}