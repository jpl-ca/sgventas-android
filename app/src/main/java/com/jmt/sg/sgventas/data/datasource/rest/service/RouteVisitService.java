package com.jmt.sg.sgventas.data.datasource.rest.service;

import android.content.Context;

import com.google.gson.JsonObject;
import com.jmt.sg.sgventas.data.datasource.rest.api.ApiService;
import com.jmt.sg.sgventas.data.model.OrderToSendEntity;
import com.jmt.sg.sgventas.data.model.RouteResponse;
import com.jmt.sg.sgventas.data.model.SearchItemResponse;

import org.json.JSONObject;

import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;
import rx.Observable;

/**
 * Created by JMTech-Android on 10/12/2015.
 */
public class RouteVisitService extends ApiService{
    private RouteVisitApi apiService;

    public RouteVisitService(Context context) {
        super(context);
        apiService = retrofit.create(RouteVisitApi.class);
    }

    public RouteVisitApi getApi() {
        return apiService;
    }

    public interface RouteVisitApi{
        @GET(_api+"/auth/trackable/me/products/tasks-visit-points/today")
        Observable<RouteResponse> getCurrentRoute();

        @GET(_api+"/auth/trackable/me/products?searchType=name")
        Observable<SearchItemResponse> searchProducts(@Query("searchTerm") String search);

        @POST(_api+"/auth/trackable/me/products/tasks-visit-points/{visit_point_id}/product-order")
        Observable<JSONObject> sendOrder(@Path("visit_point_id") long visit_point_id, @Body OrderToSendEntity body);

        @FormUrlEncoded
        @POST(_api+"/auth/trackable/me/products/tasks-visit-points/{visit_point_id}/product-order")
        Observable<JSONObject> changeOrderState(@Path("visit_point_id") long visit_point_id, @Field("state_id") int state_id);

        @FormUrlEncoded
        @POST(_api+"/auth/me/reschedule-visit-point")
        Observable<JSONObject> rescheduleVisitPoints(@Field("id") long visit_point_id,@Field("comment_of_rescheduling") String comment_of_rescheduling);
    }
}