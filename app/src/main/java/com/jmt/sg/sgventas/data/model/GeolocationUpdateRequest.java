package com.jmt.sg.sgventas.data.model;

import com.jmt.sg.sgventas.data.mapper.BaseResponse;
import java.util.ArrayList;

/**
 * Created by jmtech on 5/16/16.
 */
public class GeolocationUpdateRequest extends BaseResponse {
    ArrayList<LatLngUpdateRequest> positions;
    double lat = 0;
    double lng = 0;

    public ArrayList<LatLngUpdateRequest> getPositions() {
        return positions;
    }

    public void setPositions(ArrayList<LatLngUpdateRequest> positions) {
        this.positions = positions;
    }
}