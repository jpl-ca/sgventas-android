package com.jmt.sg.sgventas.domain.interactor;

import com.jmt.sg.sgventas.data.model.Trackeable;
import com.jmt.sg.sgventas.domain.repository.SGInfoRepository;

/**
 * Created by jmtech on 5/12/16.
 */
public class SGInfoInteractor {
    private final SGInfoRepository sgInfoDataRepository;

    public SGInfoInteractor(SGInfoRepository sgInfoDataRepository) {
        this.sgInfoDataRepository = sgInfoDataRepository;
    }

    public void saveTrackableType(Trackeable trackeable) {
        sgInfoDataRepository.saveTrackableType(trackeable);
    }


    public Trackeable getTrackableType() {
        return  sgInfoDataRepository.getTrackableType();
    }
}