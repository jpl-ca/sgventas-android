package com.jmt.sg.sgventas.domain.repository;

import com.jmt.sg.sgventas.domain.interactor.SessionInteractor;

/**
 * Created by jmtech on 5/12/16.
 */
public interface SessionRepository {
    void checkSesion(final SessionInteractor.CheckSessionCallback sessionInteractorCallback);
    void closeSesion(final SessionInteractor.CloseSessionCallback logoutCallback);
}