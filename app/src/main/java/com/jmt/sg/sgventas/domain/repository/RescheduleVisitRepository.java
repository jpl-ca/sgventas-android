package com.jmt.sg.sgventas.domain.repository;

import com.jmt.sg.sgventas.domain.interactor.RescheduleVisitInteractor;

/**
 * Created by jmtech on 5/12/16.
 */
public interface RescheduleVisitRepository {
    void rescheduleVisit(long visit_point_id, String comment, final RescheduleVisitInteractor.Callback loginIteractorCallback);
}