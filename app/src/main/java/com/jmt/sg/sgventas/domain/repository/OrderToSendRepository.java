package com.jmt.sg.sgventas.domain.repository;

import com.jmt.sg.sgventas.domain.interactor.OrderToSendInteractor;
import com.jmt.sg.sgventas.domain.model.OrderToSendE;

/**
 * Created by jmtech on 5/12/16.
 */
public interface OrderToSendRepository {

    void saveOrderToSend(long visit_id, OrderToSendE orderToSend, final OrderToSendInteractor.Callback oCallback);

    void getOrderToSend(long visit_id, final OrderToSendInteractor.Callback oCallback);

    void removeOrderToSend(long visit_id, final OrderToSendInteractor.Callback oCallback);
}