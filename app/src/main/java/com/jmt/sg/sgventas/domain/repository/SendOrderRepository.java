package com.jmt.sg.sgventas.domain.repository;

import com.jmt.sg.sgventas.domain.interactor.SendOrderInteractor;
import com.jmt.sg.sgventas.domain.model.OrderToSendE;

/**
 * Created by jmtech on 5/12/16.
 */
public interface SendOrderRepository {
    void sendOrder(long visitPointId, OrderToSendE data, final SendOrderInteractor.Callback sCallback);
}