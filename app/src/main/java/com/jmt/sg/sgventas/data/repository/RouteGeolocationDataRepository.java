package com.jmt.sg.sgventas.data.repository;

import com.jmt.sg.sgventas.data.datasource.RouteGeolocationDataSource;
import com.jmt.sg.sgventas.data.datasource.RouteGeolocationDataSourceFactory;
import com.jmt.sg.sgventas.data.model.DataSourceFactory;
import com.jmt.sg.sgventas.data.model.GeolocationUpdateRequest;
import com.jmt.sg.sgventas.data.model.LatLngUpdateRequest;
import com.jmt.sg.sgventas.domain.interactor.RouteGeolocationInteractor;
import com.jmt.sg.sgventas.domain.model.LocationHistoryE;
import com.jmt.sg.sgventas.domain.repository.RepositoryCallback;
import com.jmt.sg.sgventas.domain.repository.RouteGeolocationRepository;

import java.util.ArrayList;

/**
 * Created by jmtech on 5/13/16.
 */
public class RouteGeolocationDataRepository implements RouteGeolocationRepository {

    private static final String TAG = "LoginDataRepository";
    private final RouteGeolocationDataSourceFactory routeGeolocationDataSourceFactory;

    public RouteGeolocationDataRepository(RouteGeolocationDataSourceFactory routeGeolocationDataSourceFactory) {
        this.routeGeolocationDataSourceFactory = routeGeolocationDataSourceFactory;
    }

    @Override
    public void storeGeolocation(double lat, double lng, final RouteGeolocationInteractor.Callback rCallback) {
        RouteGeolocationDataSource geolocationDataSource = routeGeolocationDataSourceFactory.create(DataSourceFactory.CLOUD);
        geolocationDataSource.storeGeolocation(lat, lng, new RepositoryCallback() {
            @Override
            public void onSuccess(Object object) {
                rCallback.onRegisterGeolocationSuccess();
            }
            @Override
            public void onError(Object object) {
                String message = "";
                if(object!=null){
                    message= object.toString();
                }
                System.out.println(TAG+"->"+message);
                rCallback.onRegisterGeolocationError(message);
            }
        });
    }

    @Override
    public void storeGeolocation(ArrayList<LocationHistoryE> positions, final RouteGeolocationInteractor.Callback rCallback) {
        RouteGeolocationDataSource geolocationDataSource = routeGeolocationDataSourceFactory.create(DataSourceFactory.CLOUD);
        ArrayList<LatLngUpdateRequest> locations = new ArrayList<>();
        for (LocationHistoryE posLL: positions)
            locations.add(new LatLngUpdateRequest(posLL.getLat(), posLL.getLng()));
        GeolocationUpdateRequest geolocationUpdateRequest = new GeolocationUpdateRequest();
        geolocationUpdateRequest.setPositions(locations);
        geolocationDataSource.storeGeolocation(geolocationUpdateRequest, new RepositoryCallback() {
            @Override
            public void onSuccess(Object object) {
                rCallback.onRegisterGeolocationSuccess();
            }
            @Override
            public void onError(Object object) {
                String message = "";
                if(object!=null){
                    message= object.toString();
                }
                System.out.println(TAG+"->"+message);
                rCallback.onRegisterGeolocationError(message);
            }
        });
    }
}