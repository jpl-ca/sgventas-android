package com.jmt.sg.sgventas.presentation.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.jmt.sg.sgventas.R;
import com.jmt.sg.sgventas.domain.model.ItemProductE;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by jmtech on 5/20/16.
 */
public class ItemsAdapter extends RecyclerView.Adapter<ItemsAdapter.ViewHolder> {
    private Context ctx;
    private ItemsListener itemsListener;
    private ArrayList<ItemProductE> data;
    public ItemsAdapter(Context _ctx, ArrayList<ItemProductE> data, ItemsListener itemsListener){
        ctx = _ctx;
        this.data = data;
        this.itemsListener = itemsListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.lv_item_product, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        ItemProductE obj = data.get(position);
        holder.tv_nombre.setText(obj.getName());
        holder.tv_brand.setText(obj.getBrand());
        holder.tv_model.setText(obj.getModel());
        holder.tv_category.setText(obj.getCategory());

        String description = obj.getBrief_description();
        holder.tv_description.setText(description);
        String price = String.format(Locale.ENGLISH,obj.getCurrency_symbol()+" %.2f",obj.getPrice());
        holder.tv_price.setText(price);
        holder.check.setChecked(itemsListener.isItemSelected(position));
        holder.check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(((CheckBox)v).isChecked()){
                    itemsListener.onCheckItemClick(position);
                }else{
                    itemsListener.onUnCheckItemClick(position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tv_nombre,tv_price,tv_brand,tv_model,tv_category,tv_description;
        private CheckBox check;
        public ViewHolder(View itemView) {
            super(itemView);
            tv_nombre = (TextView)itemView.findViewById(R.id.tv_name);
            tv_price = (TextView)itemView.findViewById(R.id.tv_price);
            tv_brand = (TextView)itemView.findViewById(R.id.tv_brand);
            tv_model = (TextView)itemView.findViewById(R.id.tv_model);
            tv_category = (TextView)itemView.findViewById(R.id.tv_category);
            tv_description = (TextView)itemView.findViewById(R.id.tv_description);
            check = (CheckBox) itemView.findViewById(R.id.cb_checked);
        }
    }

    public interface ItemsListener{
        void onCheckItemClick(int pos);
        void onUnCheckItemClick(int pos);
        boolean isItemSelected(int pos);
    }
}