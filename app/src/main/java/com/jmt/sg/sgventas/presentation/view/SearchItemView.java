package com.jmt.sg.sgventas.presentation.view;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import com.jmt.sg.sgventas.domain.model.ItemProductE;

import java.util.ArrayList;

/**
 * Created by jmtech on 5/12/16.
 */
public interface SearchItemView extends BaseView {
    Context getContext();
    AppCompatActivity getAppActivity();
    void updateTotalItemsSelected(int total);
    void showProductList(ArrayList<ItemProductE> items);
    void showMessage(String message);
}