package com.jmt.sg.sgventas.presentation.ui.activity;

import android.content.Intent;
import android.os.Bundle;

import com.jmt.sg.sgventas.presentation.ui.fragment.LoginFragment;
import com.jmt.sg.sgventas.R;

public class LoginActivity extends BaseActivity implements LoginFragment.OnLoginListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    @Override
    public void nextActivity() {
        startActivity(new Intent(this,MapHomeActivity.class));
        finish();
    }

    @Override
    public void validateOrganizationActivity() {
        startActivity(new Intent(this,OrganizationActivity.class));
        finish();
    }
}