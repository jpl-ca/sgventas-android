package com.jmt.sg.sgventas.data.mapper;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jmt.sg.sgventas.data.model.RouteVisitResponse;
import com.jmt.sg.sgventas.domain.model.RouteVisitE;

import java.util.ArrayList;

/**
 * Created by jmtech on 5/16/16.
 */
public class RouteVisitDataMapper {
    Gson gson;

    public RouteVisitDataMapper(Gson gson){
        this.gson = gson;
    }

    public ArrayList<RouteVisitE> transformRoute(ArrayList<RouteVisitResponse> routeResponse){
        String routeStr = gson.toJson(routeResponse);
        System.out.println("A transformar: \n"+routeStr);
        ArrayList<RouteVisitE> routeVisitList = gson.fromJson(routeStr, new TypeToken<ArrayList<RouteVisitE>>(){}.getType());
        System.out.println("Transformado: \n"+gson.toJson(routeVisitList));
        return routeVisitList;
    }
}