package com.jmt.sg.sgventas.domain.interactor;

import com.jmt.sg.sgventas.domain.model.ItemProductE;
import com.jmt.sg.sgventas.domain.repository.SearchItemsRepository;

import java.util.ArrayList;

/**
 * Created by jmtech on 5/12/16.
 */
public class SearchItemsInteractor {
    private final SearchItemsRepository searchItemsRepository;

    public SearchItemsInteractor(SearchItemsRepository searchItemsRepository) {
        this.searchItemsRepository = searchItemsRepository;
    }

    public void searchItems(String val, final Callback callback) {
        searchItemsRepository.searchItems(val, callback);
    }

    public interface Callback {
        void onSearchItemsSuccess(ArrayList<ItemProductE> items);
        void onSearchItemsError(String message);
    }
}