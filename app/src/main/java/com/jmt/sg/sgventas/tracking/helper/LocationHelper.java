package com.jmt.sg.sgventas.tracking.helper;

import com.jmt.sg.sgventas.domain.model.LocationHistoryE;

import java.util.ArrayList;

/**
 * Created by jmtech on 7/6/16.
 */
public class LocationHelper {
    private static final double MIN_DISTANCE_ALLOWED = 8;
    private static final double MIN_DISTANCE_AS_MOVEMENT = 3;

    public static LocationHistoryE PI = null;
    public static final int maxLocationHistory = 5;
    public static int posLocationHistory = 0;
    public static LocationHistoryE locationHistoryE[] = new LocationHistoryE[maxLocationHistory];

    public static LocationHistoryE adjustLocation(LocationHistoryE[] locationHistoryList) {
        double last_distance = 0;
        LocationHistoryE point_valid = null;

        /** Recorrer todas las distancias */
        for (LocationHistoryE location : locationHistoryList) {

            /** Evaluar si la distancia actual es mayor que la anterior para validar que esta avanzando y que la distancia sea mayor a la minima permitida como distancia de avance */
            if(location.getDistance() < last_distance && location.getDistance() < MIN_DISTANCE_ALLOWED)
                return PI;

            last_distance = location.getDistance();
            point_valid = location;
        }

        /** Si la distancia entre el punto pivote y el ultimo punto registrado sigue siendo menor a la minima distancia permitida, se envia el punto pivote */
        if(locationHistoryList[locationHistoryList.length - 1].getDistance() < MIN_DISTANCE_AS_MOVEMENT) return PI;

        return point_valid;
    }

    public static ArrayList<LocationHistoryE> adjustLocations(LocationHistoryE[] locationHistoryList) {
        double last_distance = 0;

        ArrayList<LocationHistoryE> point_valid = new ArrayList<>();

        /** Recorrer todas las distancias */
        for (LocationHistoryE location : locationHistoryList) {

            /** Evaluar si la distancia actual es mayor que la anterior para validar que esta avanzando y que la distancia sea mayor a la minima permitida como distancia de avance */
            if(location.getDistance() < last_distance && location.getDistance() < MIN_DISTANCE_ALLOWED){
                return getDefaultLocation();
            }

            last_distance = location.getDistance();

            point_valid.add(location);
        }

        /** Si la distancia entre el punto pivote y el ultimo punto registrado sigue siendo menor a la minima distancia permitida, se envia el punto pivote */
        if(locationHistoryList[locationHistoryList.length - 1].getDistance() < MIN_DISTANCE_AS_MOVEMENT){
            return getDefaultLocation();
        }

        if(point_valid.size() == 0)return getDefaultLocation();

        System.out.println("Adjusted with:"+point_valid.size()+" points");

        return point_valid;
    }

    public static ArrayList<LocationHistoryE> getDefaultLocation(){
        ArrayList<LocationHistoryE> point_valid = new ArrayList<>();
        point_valid.add(PI);
        return point_valid;
    }

    public static double calcDistance(double lat1, double lng1, double lat2, double lng2) {
        double earthRadius = 6371000; //meters
        double dLat = Math.toRadians(lat2-lat1);
        double dLng = Math.toRadians(lng2-lng1);
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(dLng/2) * Math.sin(dLng/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        return earthRadius * c;
    }

}