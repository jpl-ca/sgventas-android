package com.jmt.sg.sgventas.domain.interactor;

import com.jmt.sg.sgventas.domain.model.LocationHistoryE;
import com.jmt.sg.sgventas.domain.repository.RouteGeolocationRepository;
import com.jmt.sg.sgventas.tracking.helper.LocationHelper;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by jmtech on 5/12/16.
 */
public class RouteGeolocationInteractor {
    private final RouteGeolocationRepository routeGeolocationRepository;

    public RouteGeolocationInteractor(RouteGeolocationRepository routeGeolocationRepository) {
        this.routeGeolocationRepository = routeGeolocationRepository;
    }

    public void storeGeolocation(double lat, double lng, final Callback placeCallback) {
//        approachOne(lat,lng,placeCallback);
        approachThree(lat, lng, placeCallback);
    }

    private void approachOne(double lat, double lng, final Callback placeCallback) {
        routeGeolocationRepository.storeGeolocation(lat,lng, placeCallback);
    }

    private void approachThree(double lat, double lng, final Callback placeCallback) {
        if(LocationHelper.PI == null){

            LocationHistoryE locationH = new LocationHistoryE(lat, lng);

            LocationHelper.PI = locationH;

            routeGeolocationRepository.storeGeolocation(lat, lng, placeCallback);
        }else{
            double latitude = LocationHelper.PI.getLat();
            double longitude = LocationHelper.PI.getLng();
            double distance = LocationHelper.calcDistance(lat, lng, latitude, longitude);

            LocationHistoryE locationH = new LocationHistoryE(lat, lng, distance);

            LocationHelper.locationHistoryE[LocationHelper.posLocationHistory++] = locationH;

            System.out.println("GPS-Dist-->"+distance);
            System.out.println("GPS-->"+ Arrays.deepToString(LocationHelper.locationHistoryE));
        }


        if(LocationHelper.posLocationHistory == LocationHelper.maxLocationHistory){
            ArrayList<LocationHistoryE> locationHistories = LocationHelper.adjustLocations(LocationHelper.locationHistoryE);

            LocationHelper.PI = locationHistories.get(locationHistories.size()-1);

            System.out.println("GPS-D send PI-->"+LocationHelper.PI);

            /**  Enviar por unidad */
//            for (LocationHistoryE locationFixed:locationHistories){
//                routeGeolocationRepository.storeGeolocation(locationFixed.getLat(), locationFixed.getLng(), placeCallback);
//            }

            /**  Enviar por conjunto */
            routeGeolocationRepository.storeGeolocation(locationHistories, placeCallback);

            LocationHelper.locationHistoryE = new LocationHistoryE[LocationHelper.maxLocationHistory];
            LocationHelper.posLocationHistory = 0;

        }

    }

    public interface Callback {
        void onRegisterGeolocationSuccess();
        void onRegisterGeolocationError(String message);
    }
}