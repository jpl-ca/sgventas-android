package com.jmt.sg.sgventas.data.datasource;

import com.jmt.sg.sgventas.data.model.Trackeable;

/**
 * Created by jmtech on 5/13/16.
 */
public interface SGInfoDataSource {
    void saveTrackeableType(Trackeable trackeable);
    Trackeable getTrackeableType();
}