package com.jmt.sg.sgventas.domain.repository;

import com.jmt.sg.sgventas.domain.interactor.ChangeOrderStateInteractor;

/**
 * Created by jmtech on 5/12/16.
 */
public interface ChangeStateOrderRepository {
    void confirmOrder(long visit_point_id, final ChangeOrderStateInteractor.CallbackConfirm callback);
    void cancelOrder(long visit_point_id, final ChangeOrderStateInteractor.CallbackCancel callback);
}