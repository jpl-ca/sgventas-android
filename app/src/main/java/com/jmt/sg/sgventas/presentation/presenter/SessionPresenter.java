package com.jmt.sg.sgventas.presentation.presenter;

import android.content.Context;
import android.os.Handler;

import com.jmt.sg.sgventas.R;
import com.jmt.sg.sgventas.data.datasource.SessionDataSourceFactory;
import com.jmt.sg.sgventas.data.mapper.TrackeableDataMapper;
import com.jmt.sg.sgventas.data.repository.SessionDataRepository;
import com.jmt.sg.sgventas.domain.interactor.SessionInteractor;
import com.jmt.sg.sgventas.domain.repository.SessionRepository;
import com.jmt.sg.sgventas.presentation.view.SessionView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Set;

/**
 * Created by jmtech on 5/12/16.
 */
public class SessionPresenter implements Presenter<SessionView>,SessionInteractor.CheckSessionCallback {

    Context context;
    SessionView sessionView;
    private SessionInteractor sessionInteractor;

    @Override
    public void addView(SessionView view) {
        sessionView = view;
        context = view.getContext();
        SessionRepository sessionRepository = new SessionDataRepository(new SessionDataSourceFactory(context),new TrackeableDataMapper());
        sessionInteractor = new SessionInteractor(sessionRepository);
    }

    @Override
    public void removeView() {
        sessionView = null;
    }

    public void getSession(){
        sessionView.checking();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                sessionInteractor.checkSession(SessionPresenter.this);
            }
        }, 1500);
    }

    @Override
    public void onValidateSuccess() {
        sessionView.hasSession(true);
    }

    @Override
    public void onValidateError(String message) {
        sessionView.hasSession(false);
        sessionView.showErrorMessage(message);
    }

    @Override
    public void onConectionError() {
        sessionView.showErrorInfo(context.getString(R.string.e_hay_problemas_de_conexion));
    }
}