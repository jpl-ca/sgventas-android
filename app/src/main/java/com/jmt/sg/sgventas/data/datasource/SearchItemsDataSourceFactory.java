package com.jmt.sg.sgventas.data.datasource;

import android.content.Context;

import com.jmt.sg.sgventas.data.datasource.rest.RestSearchItemDataSource;
import com.jmt.sg.sgventas.data.model.DataSourceFactory;

/**
 * Created by jmtech on 5/13/16.
 */
public class SearchItemsDataSourceFactory {
    private final Context context;

    public SearchItemsDataSourceFactory(Context context) {
        if (context == null) {
            throw new IllegalArgumentException("Constructor parameters cannot be null. {Context}");
        }
        this.context = context.getApplicationContext();
    }

    public SearchItemsDataSource create(DataSourceFactory dataSourceFactory) {
        SearchItemsDataSource searchItemsDataSource = null;
        switch (dataSourceFactory) {
            case CLOUD:
                searchItemsDataSource = new RestSearchItemDataSource(context);
                break;
        }
        return searchItemsDataSource;
    }
}