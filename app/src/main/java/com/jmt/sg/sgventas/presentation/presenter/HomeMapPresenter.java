package com.jmt.sg.sgventas.presentation.presenter;

import android.content.Context;
import android.location.Location;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.jmt.sg.sgventas.R;
import com.jmt.sg.sgventas.data.datasource.RouteVisitDataSourceFactory;
import com.jmt.sg.sgventas.data.mapper.RouteVisitDataMapper;
import com.jmt.sg.sgventas.data.repository.RouteVisitDataRepository;
import com.jmt.sg.sgventas.domain.interactor.RouteVisitInteractor;
import com.jmt.sg.sgventas.domain.model.RouteVisitE;
import com.jmt.sg.sgventas.domain.repository.RouteVisitRepository;
import com.jmt.sg.sgventas.presentation.utils.Constants;
import com.jmt.sg.sgventas.presentation.view.HomeMapView;
import com.jmt.sg.sgventas.tracking.invoker.GpsInvoker;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by jmtech on 5/12/16.
 */
public class HomeMapPresenter implements Presenter<HomeMapView>,RouteVisitInteractor.Callback {

    private Context context;
    private HomeMapView homeMapView;
    private RouteVisitInteractor routeVisitInteractor;

    private HashMap<String,Integer> hashVisit;
    private Marker markerVisit[];

    private GpsInvoker gpsInvoker;

    private ArrayList<RouteVisitE> visit_points;

    public HomeMapPresenter(){
        hashVisit = new HashMap<>();
        markerVisit = new Marker[0];
    }

    @Override
    public void addView(HomeMapView view) {
        homeMapView = view;
        context = view.getContext();

        RouteVisitRepository routeVisitRepository = new RouteVisitDataRepository(new RouteVisitDataSourceFactory(context),new RouteVisitDataMapper(new Gson()));
        routeVisitInteractor = new RouteVisitInteractor(routeVisitRepository);

        gpsInvoker = new GpsInvoker(context);
    }

    @Override
    public void removeView() {
        homeMapView = null;
    }

    public void getRouteVisit(){
        routeVisitInteractor.getRouteVisit(this);
    }

    @Override
    public void onGetRouteSuccess(ArrayList<RouteVisitE> routePoints) {
        if(routePoints.size()>0){
            visit_points = routePoints;
            if(homeMapView!=null)homeMapView.clearMap();
            int idx = 0;
            markerVisit = new Marker[visit_points.size()];
            for (RouteVisitE visit:visit_points) {
                int visitState = visit.getVisit_state_id();
                if(visitState == Constants.VISIT_STATE.Cancelled)continue;
                int ic_marker = getIconFromState(visitState);
                LatLng ll = new LatLng(visit.getLat(),visit.getLng());
                MarkerOptions markerOptions = new MarkerOptions().position(ll).title(visit.getName()).icon(BitmapDescriptorFactory.fromResource(ic_marker));
                addVisitMarker(idx++,markerOptions);
            }
        }else{
            homeMapView.showMessage(context.getResources().getString(R.string.error_visit_no_data_available));
        }
    }

    public void addVisitMarker(int idx,MarkerOptions markerOptions){
        if(homeMapView!=null){
            markerVisit[idx] = homeMapView.addVisitMarker(markerOptions);
            hashVisit.put(markerVisit[idx].getId(),idx);
        }
    }

    public int getIconFromState(int visitState){
        switch (visitState){
            case Constants.VISIT_STATE.Scheduled: return R.mipmap.ic_marker_sheduled;
            case Constants.VISIT_STATE.Done: return R.mipmap.ic_marker_visited;
            case Constants.VISIT_STATE.ReScheduling: return R.mipmap.ic_marker_postponed;
        }
        return R.mipmap.ic_marker_sheduled;
    }

    @Override
    public void onGetRouteError(String message) {
        if(homeMapView!=null)
            homeMapView.showMessage(message);
    }


    public void startGpsData() {
        gpsInvoker.startGpsData();
    }
    public void getLastLocation() {
        gpsInvoker.startGpsListener(new GpsInvoker.LocationCallback() {
            @Override
            public void updateLocation(Location location) {
                if(homeMapView!=null){
                    stopUpdateLocations();
                    homeMapView.firstPosition(new LatLng(location.getLatitude(), location.getLongitude()));
                }
            }
        });
    }
    public void startUpdateLocations() {
        gpsInvoker.startGpsListener(new GpsInvoker.LocationCallback() {
            @Override
            public void updateLocation(Location location) {
                if(homeMapView!=null)homeMapView.updatePosition(new LatLng(location.getLatitude(), location.getLongitude()));
            }
        });
    }
    public void stopUpdateLocations() {
        gpsInvoker.stopGpsListener();
    }
    public void stopGpsData() {
        gpsInvoker.stopGpsData();
    }

    public RouteVisitE onVisitClick(String marker_id) {
        if(!hashVisit.containsKey(marker_id))return null;
        if(visit_points==null)return null;
        return visit_points.get(hashVisit.get(marker_id));
    }

    public ArrayList<RouteVisitE> getRouteVisits() {
        return visit_points;
    }
}