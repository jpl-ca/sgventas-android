package com.jmt.sg.sgventas.domain.repository;

import com.jmt.sg.sgventas.domain.interactor.OrganizationInteractor;

/**
 * Created by jmtech on 5/12/16.
 */
public interface OrganizationRepository {
    void validateOrganization(String organization_name, final OrganizationInteractor.Callback organizationInteractorCallback);
}