package com.jmt.sg.sgventas.presentation.presenter;

import android.content.Context;

import com.google.gson.Gson;
import com.jmt.sg.sgventas.data.datasource.SearchItemsDataSourceFactory;
import com.jmt.sg.sgventas.data.mapper.ItemProductDataMapper;
import com.jmt.sg.sgventas.data.repository.SearchItemsDataRepository;
import com.jmt.sg.sgventas.domain.interactor.SearchItemsInteractor;
import com.jmt.sg.sgventas.domain.model.ItemProductE;
import com.jmt.sg.sgventas.domain.repository.SearchItemsRepository;
import com.jmt.sg.sgventas.presentation.view.SearchItemView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 * Created by jmtech on 5/12/16.
 */
public class SearchItemPresenter implements Presenter<SearchItemView>, SearchItemsInteractor.Callback{
    private Context context;
    private SearchItemView searchItemView;

    private ArrayList<ItemProductE> ItemProductList;
    private HashMap<Long, ItemProductE> itemInOrderMap;
    private HashSet<Long> idsInOrder;

    SearchItemsInteractor searchItemsInteractor;

    @Override
    public void addView(SearchItemView view) {
        searchItemView = view;
        context = view.getContext();

        ItemProductList = new ArrayList<>();
        itemInOrderMap =new HashMap<>();
        idsInOrder = new HashSet<>();
        System.out.println("INICIALIZANDO :::>>>"+idsInOrder);


        SearchItemsRepository searchItemsRepository = new SearchItemsDataRepository(new SearchItemsDataSourceFactory(context),new ItemProductDataMapper(new Gson()));
        searchItemsInteractor = new SearchItemsInteractor(searchItemsRepository);
    }

    public void searchItem(String val){
        searchItemsInteractor.searchItems(val,this);
    }

    @Override
    public void removeView() {
        searchItemView = null;
    }

    @Override
    public void onSearchItemsSuccess(ArrayList<ItemProductE> items) {
        ItemProductList = items;
        searchItemView.showProductList(ItemProductList);
    }

    @Override
    public void onSearchItemsError(String message) {
        searchItemView.showMessage(message);
    }

    public void clearSearch() {
        ItemProductList = new ArrayList<>();
        searchItemView.showProductList(ItemProductList);
    }

    public void addItemInOrder(ItemProductE itemProductE) {
        itemInOrderMap.put(itemProductE.getId(),itemProductE);
        idsInOrder.add(itemProductE.getId());
        searchItemView.updateTotalItemsSelected(idsInOrder.size());
    }

    public void removeItemInOrder(ItemProductE itemProductE) {
        itemInOrderMap.remove(itemProductE.getId());
        idsInOrder.remove(itemProductE.getId());
        searchItemView.updateTotalItemsSelected(idsInOrder.size());
        System.out.println("REMOVING :::>>>"+this.idsInOrder);
    }

    public void setIdsInOrder(HashSet<Long> idsInOrder) {
        System.out.println("UPDATING :::>>>"+this.idsInOrder);
        this.idsInOrder = idsInOrder;
        System.out.println("UPDATED TO:::>>>"+this.idsInOrder);
        searchItemView.updateTotalItemsSelected(idsInOrder.size());
    }

    public boolean isItemIdInOrder(long item_id) {
        System.out.println("CONSULTANDO :::>>>"+item_id+" IN:"+idsInOrder);
        return idsInOrder.contains(item_id);
    }

    public HashSet<Long> getIdsInOrder() {
        return idsInOrder;
    }

    public HashMap<Long, ItemProductE> getItemsInOrderMap() {
        return itemInOrderMap;
    }
}