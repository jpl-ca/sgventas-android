package com.jmt.sg.sgventas.data.datasource.db;

import android.content.Context;

import com.jmt.sg.sgventas.data.datasource.RouteGeolocationDataSource;
import com.jmt.sg.sgventas.data.model.GeolocationUpdateRequest;
import com.jmt.sg.sgventas.domain.repository.RepositoryCallback;

/**
 * Created by jmtech on 5/13/16.
 */
public class DbRouteGeolocationDataSource implements RouteGeolocationDataSource {
    private final Context context;

    public DbRouteGeolocationDataSource(Context context) {
        this.context= context;
    }

    @Override
    public void storeGeolocation(double lat, double lng, RepositoryCallback repositoryCallback) {

    }

    @Override
    public void storeGeolocation(GeolocationUpdateRequest geolocationUpdateRequest, RepositoryCallback repositoryCallback) {

    }
}