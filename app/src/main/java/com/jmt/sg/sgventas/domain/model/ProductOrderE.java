package com.jmt.sg.sgventas.domain.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by jmtech on 5/16/16.
 */
public class ProductOrderE implements Serializable {
    private long id;

    private String description;

    private String status;

    private int state_id;

    private ArrayList<ItemInOrderE> product_order_items;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<ItemInOrderE> getProduct_order_items() {
        return product_order_items;
    }

    public void setProduct_order_items(ArrayList<ItemInOrderE> product_order_items) {
        this.product_order_items = product_order_items;
    }

    public int getState_id() {
        return state_id;
    }

    public void setState_id(int state_id) {
        this.state_id = state_id;
    }
}