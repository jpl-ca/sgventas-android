package com.jmt.sg.sgventas.presentation.ui.activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;

import com.jmt.sg.sgventas.R;
import com.jmt.sg.sgventas.domain.model.RouteVisitE;
import com.jmt.sg.sgventas.presentation.ui.fragment.MapHomeFragment;
import com.jmt.sg.sgventas.presentation.ui.fragment.TimelineFragment;
import com.jmt.sg.sgventas.presentation.ui.fragment.VisitOrderFragment;
import com.jmt.sg.sgventas.tracking.util.Utility;

import java.util.ArrayList;
import java.util.Arrays;

public class MapHomeActivity extends BaseActivity implements MapHomeFragment.OnHomeMapListener {

    private final MapHomeFragment mapHomeFragment = MapHomeFragment.instance();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_home);
        showFragment();
    }

    private void showFragment() {
        getSupportFragmentManager().beginTransaction().replace(R.id.container, mapHomeFragment).commit();
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case Utility.REQUEST_CODE : mapHomeFragment.onRequestPermissionsResult(requestCode,resultCode);break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utility.REQUEST_CODE : mapHomeFragment.onRequestPermissionsResult(requestCode,permissions,grantResults);break;
        }
    }

    @Override
    public void profileActivity() {
        Intent it = new Intent(this,ProfileActivity.class);
        startActivity(it);
    }

    @Override
    public void visitDetailActivity(RouteVisitE routeVisit) {
        Intent it = new Intent(this,VisitOrderActivity.class);
        it.putExtra(VisitOrderFragment.ROUTE_VISIT_DATA,routeVisit);
        startActivityForResult(it,VisitOrderActivity.CODE_RESULT);
    }

    @Override
    public void timeLineActivity(ArrayList<RouteVisitE> routeVisits) {
        Intent it = new Intent(this,TimelineActivity.class);
        it.putExtra(TimelineFragment.ROUTE_VISITS_DATA,routeVisits);
        startActivityForResult(it,VisitOrderActivity.CODE_RESULT);
    }
}