package com.jmt.sg.sgventas.tracking.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.jmt.sg.sgventas.tracking.GpsConstant;
import com.jmt.sg.sgventas.tracking.util.Utility;

/**
 * Created by jmtech on 5/23/16.
 */
public class LocationFusedService extends Service implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    protected static final String TAG = "LocationFusedService";

    public static long UPDATE_INTERVAL_IN_MILLISECONDS = 15 * 1000;
    public static long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = 10 * 1000;
    protected GoogleApiClient mGoogleApiClient;
    protected LocationRequest mLocationRequest;

    protected Context context;
    protected PowerManager.WakeLock cpuWakeLock;

    IBinder mBinder = new LocalBinder();

    public class LocalBinder extends Binder {
        public LocationFusedService getServerInstance() {
            return LocationFusedService.this;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        buildGoogleApiClient();
        mGoogleApiClient.connect();

//        //in onCreate of your service
//        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
//        cpuWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,"gps_service");
//        cpuWakeLock.acquire();
//        TODO agregar en el caso de que se requiera
//        // Release in onDestroy of your service
//        if (cpuWakeLock.isHeld())
//            cpuWakeLock.release();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mGoogleApiClient.isConnected()) {
                    System.out.println(TAG + " onStartCommand: GoogleApiClient Connected");
                }
                if (!mGoogleApiClient.isConnected() || !mGoogleApiClient.isConnecting()) {
                    System.out.println(TAG + " onStartCommand GoogleApiClient not Connected");
                    mGoogleApiClient.connect();
                }
            }
        }, 1000);

        return START_STICKY;
    }

    protected synchronized void buildGoogleApiClient() {
        Log.i(TAG, "Building GoogleApiClient");
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        createLocationRequest();
    }

    protected void createLocationRequest() {
        Log.i(TAG, "createLocationRequest()");
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    protected void startLocationUpdates() {
        Log.i(TAG, "Started Location Updates");

        boolean needPermissionGranted = Utility.checkLocationPermission(context);
        if (needPermissionGranted) return;

        Log.i(TAG, "Started Location Updatessss");

        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    protected void stopLocationUpdates() {
        Log.i(TAG,"Stopped Location Updates");
        if(mGoogleApiClient.isConnected())
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        Log.i(TAG, "Connected to GoogleApiClient");
        startLocationUpdates();
    }

    /**
     * Callback that fires when the location changes.
     */
    @Override
    public void onLocationChanged(Location location) {
        if(location == null) return;
        GpsConstant.gpsLocation = location;
        Intent intentR = new Intent();
        intentR.setAction(GpsConstant.RECEIVE_GPS);
        sendBroadcast(intentR);
        Log.i("LOCATION_FUSED_SERVICE",location.getLatitude()+","+location.getLongitude()+" | spd:"+location.getSpeed());
    }

    @Override
    public void onConnectionSuspended(int cause) {
        Log.i(TAG, "Connection suspended");
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopLocationUpdates();
    }
}