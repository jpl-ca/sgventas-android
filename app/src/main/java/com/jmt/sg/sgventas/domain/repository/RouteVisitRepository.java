package com.jmt.sg.sgventas.domain.repository;

import com.jmt.sg.sgventas.domain.interactor.RouteVisitInteractor;

/**
 * Created by jmtech on 5/12/16.
 */
public interface RouteVisitRepository {
    void getRouteVisit(final RouteVisitInteractor.Callback rouInteractorCallback);
}