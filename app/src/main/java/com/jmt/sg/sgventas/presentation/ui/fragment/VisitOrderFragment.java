package com.jmt.sg.sgventas.presentation.ui.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.google.android.gms.maps.model.LatLng;
import com.jmt.sg.sgventas.R;
import com.jmt.sg.sgventas.domain.model.ItemInOrderE;
import com.jmt.sg.sgventas.domain.model.RouteVisitE;
import com.jmt.sg.sgventas.presentation.adapter.ItemsInOrderAdapter;
import com.jmt.sg.sgventas.presentation.presenter.ConfirmDialogPresenter;
import com.jmt.sg.sgventas.presentation.presenter.VisitOrderPresenter;
import com.jmt.sg.sgventas.presentation.utils.Constants;
import com.jmt.sg.sgventas.presentation.view.ConfirmDialogView;
import com.jmt.sg.sgventas.presentation.view.VisitOrderView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Locale;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by JMTech-Android on 29/12/2015.
 */
public class VisitOrderFragment extends BaseFragment implements VisitOrderView{
    @BindString(R.string.t_detalle_visita) protected String titlebar;
    @Bind(R.id.ll_root) LinearLayout ll_root;
    @Bind(R.id.toolbar) protected Toolbar toolbar;

    @Bind(R.id.txt_cliente) protected TextView txt_cliente;
    @Bind(R.id.txt_telefono) protected TextView txt_telefono;
    @Bind(R.id.txt_direccion) protected TextView txt_direccion;
    @Bind(R.id.txt_referencia) protected TextView txt_referencia;
    @Bind(R.id.txt_total_message) protected TextView txt_total_message;
    @Bind(R.id.txt_tiempo) protected TextView txt_tiempo;
    @Bind(R.id.txt_total_order) protected TextView txt_total_order;
    @Bind(R.id.rv_items) protected RecyclerView rv_items;
    @Bind(R.id.tv_state_visit_point) protected TextView tv_state_visit_point;
    @Bind(R.id.tv_state_product_order) protected TextView tv_state_product_order;
    @Bind(R.id.fab_menu_options) protected FloatingActionsMenu fab_menu_options;
    @Bind(R.id.fab_send_order) protected FloatingActionButton fab_send_order;
    @Bind(R.id.fab_confirm_order) protected FloatingActionButton fab_confirm_order;
    @Bind(R.id.fab_cancel_order) protected FloatingActionButton fab_cancel_order;
    @Bind(R.id.btn_reschedule) protected Button btn_reschedule;

    LayoutInflater inflater;
    private Context context;
    private OnVisitOrderListener presenter;
    private ItemsInOrderAdapter itemsInOrderAdapter;

    private ConfirmDialogPresenter confirmDialogPresenter;
    private MenuItem menuFindItem;

    public static final String ROUTE_VISIT_DATA = "RouteVisitData";
    public VisitOrderPresenter visitOrderPresenter;

    private int VISIT_POINT_STATE = -1;
    private int ORDER_STATE = -1;

    public static VisitOrderFragment instance(){
        return new VisitOrderFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_visit_order, container, false);
        ButterKnife.bind(this, rootView);
        setToolbar(toolbar, titlebar);
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setHasOptionsMenu(true);

        this.inflater = inflater;
        this.context = getContext();

        rv_items.setHasFixedSize(true);
        rv_items.setLayoutManager(new LinearLayoutManager(context));

        visitOrderPresenter = new VisitOrderPresenter(getAppActivity().getIntent());
        visitOrderPresenter.addView(this);

        return rootView;
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        if (context instanceof OnVisitOrderListener) {
            presenter = (OnVisitOrderListener) context;
        } else {
            throw new ClassCastException("debe implementar On?Listener");
        }
    }

    @Override
    public void onStart(){
        super.onStart();
    }

    @OnClick(R.id.btn_goto_direcction)
    protected void goToDirection() {
        visitOrderPresenter.getMyLocation(new VisitOrderPresenter.onGetMyLocation() {
            @Override
            public void setMyLocation(LatLng latlng) {
                RouteVisitE visit = visitOrderPresenter.getRouteVisit();

                double MeLat = latlng.latitude;
                double MeLng = latlng.longitude;

                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?saddr="+MeLat+","+MeLng+"&daddr="+visit.getLat()+","+visit.getLng()));
                intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                startActivity(intent);
            }
        });
    }

    @OnClick(R.id.btn_reschedule)
    protected void reschedule() {
        confirmDialogPresenter = new ConfirmDialogPresenter(new ConfirmDialogView() {
            @Override
            public void clickConfirm() {}
            @Override
            public void clickConfirm(String input) {
                visitOrderPresenter.rescheduleVisitPoint(input);
                fab_menu_options.collapse();
            }
            @Override
            public void clickCancel() {}
            @Override
            public Context getContext() {
                return context;
            }
        });
        confirmDialogPresenter.showInputDialog(context.getString(R.string.s_se_guardara_como_reprogramado));
    }

    @OnClick(R.id.fab_confirm_order)
    protected void confirmOrder() {
        confirmDialogPresenter = new ConfirmDialogPresenter(new ConfirmDialogView() {
            @Override
            public void clickConfirm() {
                visitOrderPresenter.confirmOrder();
                fab_menu_options.collapse();
            }
            @Override
            public void clickConfirm(String input) {}
            @Override
            public void clickCancel() {}
            @Override
            public Context getContext() {
                return context;
            }
        });
        confirmDialogPresenter.showDialog(context.getString(R.string.s_confirma_orden_pedido));
    }

    @OnClick(R.id.fab_cancel_order)
    protected void cancelOrder() {
        confirmDialogPresenter = new ConfirmDialogPresenter(new ConfirmDialogView() {
            @Override
            public void clickConfirm() {
                visitOrderPresenter.cancelOrder();
                fab_menu_options.collapse();
            }
            @Override
            public void clickConfirm(String input) {}
            @Override
            public void clickCancel() {}
            @Override
            public Context getContext() {
                return context;
            }
        });
        confirmDialogPresenter.showDialog(context.getString(R.string.s_va_a_cancelar_orden_pedido));
    }

    @OnClick(R.id.fab_send_order)
    protected void sendOrder() {
        visitOrderPresenter.sendCurrentOrder();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case android.R.id.home:presenter.onBackPressed();break;
            case R.id.action_add_item:loadItemsActivity();break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_visit, menu);
        menuFindItem = menu.findItem(R.id.action_add_item);
        if(VISIT_POINT_STATE != -1) setVisitPointState(VISIT_POINT_STATE);
        if(ORDER_STATE != -1) setOrderState(ORDER_STATE);
    }

    private void loadItemsActivity() {
        presenter.loadItemsActivity(visitOrderPresenter.getItemIds());
    }

    public void settingData(RouteVisitE routeVisitE) {
        txt_cliente.setText(routeVisitE.getName());
        txt_telefono.setText(routeVisitE.getPhone());
        txt_direccion.setText(routeVisitE.getAddress());
        txt_referencia.setText(routeVisitE.getReference());

        String hora = routeVisitE.getTime();
        txt_tiempo.setText(hora != null?hora:"00:00:00");
    }

    @Override
    public void showItemsInOrder(final ArrayList<ItemInOrderE> itemInOrder) {
        if(itemInOrder.size()==0){
            txt_total_order.setVisibility(View.GONE);
            txt_total_message.setVisibility(View.GONE);
        } else {
            fab_send_order.setVisibility(View.VISIBLE);
            txt_total_order.setVisibility(View.VISIBLE);
            txt_total_message.setVisibility(View.VISIBLE);
        }
        setVisitPointState(VISIT_POINT_STATE);
        setOrderState(ORDER_STATE);

        System.out.println("SZ :"+itemInOrder.size());

        boolean rv_editable = ORDER_STATE != Constants.ORDER_STATE.Confirmed;

        itemsInOrderAdapter = new ItemsInOrderAdapter(context, itemInOrder, rv_editable, new ItemsInOrderAdapter.ItemsOrderListener() {
            @Override
            public void onRemoveItemClick(int pos) {
                visitOrderPresenter.removeItemInOrder(pos);
            }
            @Override
            public void onUpdateAmmount(int pos, int ammount) {
                visitOrderPresenter.updateItemInOrder(pos,ammount);
            }
        });
        rv_items.setAdapter(itemsInOrderAdapter);
    }

    @Override
    public void setVisitPointState(int state) {
        int itemsInOrderSize = visitOrderPresenter.getItemInOrderSize();

        System.out.println("SIZ IN VISIT POINT ->"+itemsInOrderSize);

        VISIT_POINT_STATE = state;

        if(VISIT_POINT_STATE == Constants.VISIT_STATE.ReScheduling){
            fab_send_order.setEnabled(false);
            fab_cancel_order.setEnabled(false);
            fab_confirm_order.setEnabled(false);
            btn_reschedule.setEnabled(false);

            if(menuFindItem != null) menuFindItem.setEnabled(false);

            tv_state_visit_point.setText(getString(R.string.state_reprogramado));
        }

        else if(VISIT_POINT_STATE == Constants.VISIT_STATE.Scheduled){
            if(itemsInOrderSize == 0){
                fab_send_order.setEnabled(false);
                fab_confirm_order.setEnabled(false);
            }else{
                fab_send_order.setEnabled(true);
                fab_confirm_order.setEnabled(true);
            }
            btn_reschedule.setEnabled(true);

            tv_state_visit_point.setText(getString(R.string.state_programado));

            if(menuFindItem != null) menuFindItem.setEnabled(true);

            fab_cancel_order.setEnabled(false);
        }

        else if(VISIT_POINT_STATE == Constants.VISIT_STATE.Done){
            if(itemsInOrderSize == 0){
                fab_send_order.setEnabled(false);
                fab_confirm_order.setEnabled(false);
            }else{
                fab_send_order.setEnabled(true);
                fab_confirm_order.setEnabled(true);
            }
            btn_reschedule.setEnabled(true);

            tv_state_visit_point.setText(getString(R.string.state_visitado));

            if(menuFindItem != null) menuFindItem.setEnabled(true);

            fab_cancel_order.setEnabled(true);
        }
    }

    @Override
    public void setOrderState(int order_state_id) {
        if(VISIT_POINT_STATE == Constants.VISIT_STATE.ReScheduling) return;

        int itemsInOrderSize = visitOrderPresenter.getItemInOrderSize();
        System.out.println("SIZ IN ORDER ->"+itemsInOrderSize);
        ORDER_STATE = order_state_id;

        if(ORDER_STATE == Constants.ORDER_STATE.New){
            if(itemsInOrderSize == 0){
                fab_send_order.setEnabled(false);
            }else{
                fab_send_order.setEnabled(true);
            }
            fab_confirm_order.setEnabled(false);
            fab_cancel_order.setEnabled(false);
            btn_reschedule.setEnabled(true);
            if(menuFindItem != null) menuFindItem.setEnabled(true);

            tv_state_product_order.setText(getString(R.string.state_new));
        }

        else if(ORDER_STATE == Constants.ORDER_STATE.Done){
            if(itemsInOrderSize == 0){
                fab_send_order.setEnabled(false);
                fab_confirm_order.setEnabled(false);
            }else{
                fab_send_order.setEnabled(true);
                fab_confirm_order.setEnabled(true);
            }
            fab_cancel_order.setEnabled(true);
            btn_reschedule.setEnabled(true);
            if(menuFindItem != null) menuFindItem.setEnabled(true);

            tv_state_product_order.setText(getString(R.string.state_sent));
        }

        else if(ORDER_STATE == Constants.ORDER_STATE.Pending){
            if(itemsInOrderSize == 0){
                fab_send_order.setEnabled(false);
                fab_confirm_order.setEnabled(false);
                fab_cancel_order.setEnabled(false);
            }else{
                fab_send_order.setEnabled(true);
                fab_confirm_order.setEnabled(true);
                fab_cancel_order.setEnabled(true);
            }
            btn_reschedule.setEnabled(true);
            if(menuFindItem != null) menuFindItem.setEnabled(true);

            tv_state_product_order.setText(getString(R.string.state_pending));
        }

        else if(ORDER_STATE == Constants.ORDER_STATE.Anulled){
            if(itemsInOrderSize == 0){
                fab_send_order.setEnabled(false);
            }else{
                fab_send_order.setEnabled(true);
            }
            fab_confirm_order.setEnabled(false);
            fab_cancel_order.setEnabled(false);

            btn_reschedule.setEnabled(true);
            if(menuFindItem != null) menuFindItem.setEnabled(true);

            tv_state_product_order.setText(getString(R.string.state_anulled));
        }

        else if(ORDER_STATE == Constants.ORDER_STATE.Confirmed){
            fab_send_order.setEnabled(false);
            fab_confirm_order.setEnabled(false);
            fab_cancel_order.setEnabled(false);

            btn_reschedule.setEnabled(false);
            if(menuFindItem != null) menuFindItem.setEnabled(false);

            tv_state_product_order.setText(getString(R.string.state_confirmed));
        }
    }

    @Override
    public void showMessage(String message) {
        showMessage(ll_root,message);
    }

    @Override
    public void orderListSent() {
        fab_menu_options.collapse();
    }

    @Override
    public void onStateChangedToRefresh() {
        presenter.sendResultSuccess(visitOrderPresenter.getRouteVisit());
    }

    @Override
    public void setTotalValue(String symbol, double total) {
        String str_total = "TOTAL: " + symbol + " "+ String.format(Locale.ENGLISH,"%.2f",total);
        txt_total_order.setText(str_total);
    }

    @Override
    public void onStop() {
        super.onStop();
        fab_menu_options.collapse();
    }

    public void updateItemList(Intent data) {
        visitOrderPresenter.reloadItemsToOrder(data);
    }

    public void updateItemToOrderSelected(Intent data) {
        visitOrderPresenter.reloadItemsToOrderSelected(data);
    }

    public interface OnVisitOrderListener{
        void sendResultSuccess(RouteVisitE routeVisit);
        void loadItemsActivity(HashSet<Long> item_ids);
        void onBackPressed();
    }
}