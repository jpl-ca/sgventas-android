package com.jmt.sg.sgventas.domain.repository;

import com.jmt.sg.sgventas.domain.interactor.SearchItemsInteractor;

/**
 * Created by jmtech on 5/12/16.
 */
public interface SearchItemsRepository {
    void searchItems(String name, final SearchItemsInteractor.Callback callback);
}