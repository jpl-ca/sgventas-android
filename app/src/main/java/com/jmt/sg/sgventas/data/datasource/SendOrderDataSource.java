package com.jmt.sg.sgventas.data.datasource;

import com.jmt.sg.sgventas.data.model.OrderToSendEntity;
import com.jmt.sg.sgventas.domain.repository.RepositoryCallback;

/**
 * Created by jmtech on 5/13/16.
 */
public interface SendOrderDataSource {
    void sendOrder(long visitPointId, OrderToSendEntity data, RepositoryCallback repositoryCallback);
}