package com.jmt.sg.sgventas.presentation.view;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import com.jmt.sg.sgventas.domain.model.ItemInOrderE;
import com.jmt.sg.sgventas.domain.model.RouteVisitE;

import java.util.ArrayList;

/**
 * Created by jmtech on 5/12/16.
 */
public interface VisitOrderView extends BaseView {
    Context getContext();
    AppCompatActivity getAppActivity();
    void settingData(RouteVisitE routeVisitE);
    void showItemsInOrder(ArrayList<ItemInOrderE> itemInOrder);
    void setVisitPointState(int state);
    void showMessage(String message);
    void orderListSent();
    void onStateChangedToRefresh();
    void setTotalValue(String symbol, double total);
    void setOrderState(int order_state_id);
}