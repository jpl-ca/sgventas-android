package com.jmt.sg.sgventas.data.datasource;

import android.content.Context;

import com.jmt.sg.sgventas.data.datasource.rest.RestSessionDataStore;
import com.jmt.sg.sgventas.data.model.DataSourceFactory;

/**
 * Created by jmtech on 5/13/16.
 */
public class SessionDataSourceFactory {
    private final Context context;

    public SessionDataSourceFactory(Context context) {
        if (context == null) {
            throw new IllegalArgumentException("Constructor parameters cannot be null. {Context}");
        }
        this.context = context.getApplicationContext();
    }

    public SessionDataSource create(DataSourceFactory dataSourceFactory) {
        SessionDataSource sessionDataSource = null;
        switch (dataSourceFactory) {
            case CLOUD:
                sessionDataSource = new RestSessionDataStore(context);
                break;
        }
        return sessionDataSource;
    }
}