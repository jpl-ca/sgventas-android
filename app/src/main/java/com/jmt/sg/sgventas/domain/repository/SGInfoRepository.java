package com.jmt.sg.sgventas.domain.repository;

import com.jmt.sg.sgventas.data.model.Trackeable;

/**
 * Created by jmtech on 5/12/16.
 */
public interface SGInfoRepository {
    void saveTrackableType(Trackeable trackeable);
    Trackeable getTrackableType();
}