package com.jmt.sg.sgventas.data.datasource;

import android.content.Context;

import com.jmt.sg.sgventas.data.datasource.rest.RestSendOrderDataSource;
import com.jmt.sg.sgventas.data.model.DataSourceFactory;

/**
 * Created by jmtech on 5/13/16.
 */
public class SendOrderDataSourceFactory {
    private final Context context;

    public SendOrderDataSourceFactory(Context context) {
        if (context == null) {
            throw new IllegalArgumentException("Constructor parameters cannot be null. {Context}");
        }
        this.context = context.getApplicationContext();
    }

    public SendOrderDataSource create(DataSourceFactory dataSourceFactory) {
        SendOrderDataSource sendOrderDataSource = null;
        switch (dataSourceFactory) {
            case CLOUD:
                sendOrderDataSource = new RestSendOrderDataSource(context);
                break;
        }
        return sendOrderDataSource;
    }
}