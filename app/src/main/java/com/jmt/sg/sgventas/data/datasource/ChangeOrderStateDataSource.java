package com.jmt.sg.sgventas.data.datasource;

import com.jmt.sg.sgventas.domain.repository.RepositoryCallback;

/**
 * Created by jmtech on 5/13/16.
 */
public interface ChangeOrderStateDataSource {
    void confirmOrder(long visit_point_id, final RepositoryCallback repositoryCallback);
    void cancelOrder(long visit_point_id, final RepositoryCallback repositoryCallback);
}