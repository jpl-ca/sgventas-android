package com.jmt.sg.sgventas.domain.repository;

import com.jmt.sg.sgventas.domain.interactor.TrackerLoadInteractor;
import com.jmt.sg.sgventas.domain.model.AgentTrackeable;

/**
 * Created by jmtech on 5/12/16.
 */
public interface TrackerLoadDataRepository {
    void loadAgentData(TrackerLoadInteractor.Callback aCallback);
}