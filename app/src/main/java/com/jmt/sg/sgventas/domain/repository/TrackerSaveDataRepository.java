package com.jmt.sg.sgventas.domain.repository;

import com.jmt.sg.sgventas.domain.interactor.TrackerSaveInteractor;
import com.jmt.sg.sgventas.domain.model.AgentTrackeable;

/**
 * Created by jmtech on 5/12/16.
 */
public interface TrackerSaveDataRepository {
    void saveTrackerData(AgentTrackeable aTrackeable, TrackerSaveInteractor.Callback aCallback);
}