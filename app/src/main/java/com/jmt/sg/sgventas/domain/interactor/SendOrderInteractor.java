package com.jmt.sg.sgventas.domain.interactor;

import com.jmt.sg.sgventas.domain.model.OrderToSendE;
import com.jmt.sg.sgventas.domain.repository.SendOrderRepository;

/**
 * Created by jmtech on 5/12/16.
 */
public class SendOrderInteractor {
    private final SendOrderRepository sendOrderRepository;

    public SendOrderInteractor(SendOrderRepository sendOrderRepository) {
        this.sendOrderRepository = sendOrderRepository;
    }

    public void sendOrder(long visitPointId, OrderToSendE data, final Callback callback) {
        sendOrderRepository.sendOrder(visitPointId, data, callback);
    }

    public interface Callback {
        void onSendOrderSuccess();
        void onSendOrderError(String message);
    }
}