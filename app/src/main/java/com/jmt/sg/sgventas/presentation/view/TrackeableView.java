package com.jmt.sg.sgventas.presentation.view;

import com.jmt.sg.sgventas.domain.model.AgentTrackeable;

/**
 * Created by jmtech on 5/12/16.
 */
public interface TrackeableView extends BaseView {
    void showData(AgentTrackeable agentTrackeable);
    void showMessage(String message);
}