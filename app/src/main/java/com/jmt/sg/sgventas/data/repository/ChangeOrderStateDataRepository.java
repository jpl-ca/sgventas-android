package com.jmt.sg.sgventas.data.repository;

import com.jmt.sg.sgventas.data.datasource.ChangeOrderStateDataSource;
import com.jmt.sg.sgventas.data.datasource.ChangeOrderStateDataSourceFactory;
import com.jmt.sg.sgventas.data.datasource.RescheduleVisitDataSource;
import com.jmt.sg.sgventas.data.datasource.RescheduleVisitDataSourceFactory;
import com.jmt.sg.sgventas.data.model.DataSourceFactory;
import com.jmt.sg.sgventas.domain.interactor.ChangeOrderStateInteractor;
import com.jmt.sg.sgventas.domain.interactor.RescheduleVisitInteractor;
import com.jmt.sg.sgventas.domain.repository.ChangeStateOrderRepository;
import com.jmt.sg.sgventas.domain.repository.RepositoryCallback;
import com.jmt.sg.sgventas.domain.repository.RescheduleVisitRepository;

/**
 * Created by jmtech on 5/13/16.
 */
public class ChangeOrderStateDataRepository implements ChangeStateOrderRepository {

    private final ChangeOrderStateDataSourceFactory changeOrderStateDataSourceFactory;

    public ChangeOrderStateDataRepository(ChangeOrderStateDataSourceFactory changeOrderStateDataSourceFactory) {
        this.changeOrderStateDataSourceFactory = changeOrderStateDataSourceFactory;
    }

    @Override
    public void confirmOrder(long visit_point_id, final ChangeOrderStateInteractor.CallbackConfirm callback) {
        ChangeOrderStateDataSource changeOrderStateDataSource = changeOrderStateDataSourceFactory.create(DataSourceFactory.CLOUD);
        changeOrderStateDataSource.confirmOrder(visit_point_id,new RepositoryCallback() {
            @Override
            public void onSuccess(Object object) {
                callback.onConfirmSuccess();
            }
            @Override
            public void onError(Object object) {
                String message = "";
                if(object!=null){
                    message= object.toString();
                }
                callback.onConfirmError(message);
            }
        });
    }

    @Override
    public void cancelOrder(long visit_point_id, final ChangeOrderStateInteractor.CallbackCancel callback) {
        ChangeOrderStateDataSource changeOrderStateDataSource = changeOrderStateDataSourceFactory.create(DataSourceFactory.CLOUD);
        changeOrderStateDataSource.cancelOrder(visit_point_id,new RepositoryCallback() {
            @Override
            public void onSuccess(Object object) {
                callback.onCancelSuccess();
            }
            @Override
            public void onError(Object object) {
                String message = "";
                if(object!=null){
                    message= object.toString();
                }
                callback.onCancelError(message);
            }
        });
    }
}