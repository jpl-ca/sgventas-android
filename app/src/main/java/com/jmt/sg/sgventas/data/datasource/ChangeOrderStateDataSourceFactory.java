package com.jmt.sg.sgventas.data.datasource;

import android.content.Context;

import com.jmt.sg.sgventas.data.datasource.rest.RestChangeOrderStateDataSource;
import com.jmt.sg.sgventas.data.datasource.rest.RestRescheduleVisitDataSource;
import com.jmt.sg.sgventas.data.model.DataSourceFactory;

/**
 * Created by jmtech on 5/13/16.
 */
public class ChangeOrderStateDataSourceFactory {
    private final Context context;

    public ChangeOrderStateDataSourceFactory(Context context) {
        if (context == null) {
            throw new IllegalArgumentException("Constructor parameters cannot be null. {Context}");
        }
        this.context = context.getApplicationContext();
    }

    public ChangeOrderStateDataSource create(DataSourceFactory dataSource) {
        ChangeOrderStateDataSource changeOrderStateDataSource = null;
        switch (dataSource) {
            case CLOUD:
                changeOrderStateDataSource = new RestChangeOrderStateDataSource(context);
                break;
        }
        return changeOrderStateDataSource;
    }
}