package com.jmt.sg.sgventas.data.datasource.db.realm;

import com.jmt.sg.sgventas.data.model.OrderToSendEntity;
import com.jmt.sg.sgventas.domain.model.AgentTrackeable;

import rx.Observable;

/**
 * Created by JMTech-Android on 26/01/2016.
 */
public interface OrderToSendDataServiceI {
    Observable<OrderToSendEntity> getOrderToSend(long visit_id);
    Observable<Boolean> registerOrderToSend(long visit_id, OrderToSendEntity user);
    Observable<Boolean> removeOrderToSend(long visit_id);
}