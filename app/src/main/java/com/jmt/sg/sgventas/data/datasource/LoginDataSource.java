package com.jmt.sg.sgventas.data.datasource;

import com.jmt.sg.sgventas.domain.repository.RepositoryCallback;

/**
 * Created by jmtech on 5/13/16.
 */
public interface LoginDataSource {
    void loginAgent(String identification_code, String password, RepositoryCallback repositoryCallback);
}