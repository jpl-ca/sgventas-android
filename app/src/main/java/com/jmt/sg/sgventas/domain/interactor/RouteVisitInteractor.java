package com.jmt.sg.sgventas.domain.interactor;

import com.jmt.sg.sgventas.domain.model.RouteVisitE;
import com.jmt.sg.sgventas.domain.repository.RouteVisitRepository;

import java.util.ArrayList;

/**
 * Created by jmtech on 5/25/16.
 */
public class RouteVisitInteractor {
    private final RouteVisitRepository routeVisitRepository;

    public RouteVisitInteractor(RouteVisitRepository routeVisitRepository) {
        this.routeVisitRepository = routeVisitRepository;
    }

    public void getRouteVisit(final Callback callback) {
        routeVisitRepository.getRouteVisit(callback);
    }

    public interface Callback {
        void onGetRouteSuccess(ArrayList<RouteVisitE> routeVisitList);
        void onGetRouteError(String message);
    }
}