package com.jmt.sg.sgventas.presentation.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jmt.sg.sgventas.R;
import com.jmt.sg.sgventas.presentation.presenter.SessionPresenter;
import com.jmt.sg.sgventas.presentation.view.SessionView;

import butterknife.Bind;

/**
 * A placeholder fragment containing a simple view.
 */
public class SplashFragment extends BaseFragment implements SessionView {
    @Bind(R.id.rl_root) RelativeLayout ll_root;
    @Bind(R.id.txt_message) TextView txt_message;

    private OnSplashListener presenter;
    private SessionPresenter sessionPresenter;

    public static SplashFragment instance(){
        return new SplashFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_splash, container, false);
        injectView(rootView);

        sessionPresenter = new SessionPresenter();
        sessionPresenter.addView(this);

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        sessionPresenter.getSession();
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        if (context instanceof OnSplashListener) {
            presenter = (OnSplashListener) context;
        } else {
            throw new ClassCastException("debe implementar On?Listener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        sessionPresenter.removeView();
    }

    @Override
    public void showErrorMessage(String message) {
        showMessage(ll_root,message);
    }

    @Override
    public void showErrorInfo(String mess) {
        txt_message.setText(mess);
        txt_message.setVisibility(View.VISIBLE);
    }

    @Override
    public void checking() {

    }

    @Override
    public void hasSession(boolean b) {
        if(b) {
            presenter.toHomeActivity();
        } else {
            presenter.toLoginActivity();
        }
    }

    public interface OnSplashListener{
        void toLoginActivity();
        void toHomeActivity();
    }
}