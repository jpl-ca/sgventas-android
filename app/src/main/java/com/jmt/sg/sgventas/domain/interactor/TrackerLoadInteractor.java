package com.jmt.sg.sgventas.domain.interactor;

import com.jmt.sg.sgventas.domain.model.AgentTrackeable;
import com.jmt.sg.sgventas.domain.repository.TrackerLoadDataRepository;

/**
 * Created by jmtech on 5/12/16.
 */
public class TrackerLoadInteractor {
    private final TrackerLoadDataRepository trackerRepository;

    public TrackerLoadInteractor(TrackerLoadDataRepository trackerRepository) {
        this.trackerRepository = trackerRepository;
    }

    public void loadAgentData(final Callback aCallback) {
        trackerRepository.loadAgentData(aCallback);
    }

    public interface Callback {
        void onLoadAgentData(AgentTrackeable agentTrackeable);
        void onLoadDataError(String message);
    }
}