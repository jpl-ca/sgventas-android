package com.jmt.sg.sgventas.presentation.ui.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.jmt.sg.sgventas.R;
import com.jmt.sg.sgventas.domain.model.ItemProductE;
import com.jmt.sg.sgventas.domain.model.RouteVisitE;
import com.jmt.sg.sgventas.presentation.adapter.ItemsAdapter;
import com.jmt.sg.sgventas.presentation.adapter.TimelineAdapter;
import com.jmt.sg.sgventas.presentation.presenter.SearchItemPresenter;
import com.jmt.sg.sgventas.presentation.presenter.VisitOrderPresenter;
import com.jmt.sg.sgventas.presentation.view.SearchItemView;
import com.jmt.sg.sgventas.presentation.view.VisitOrderView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by JMTech-Android on 29/12/2015.
 */
public class SearchItemFragment extends BaseFragment implements SearchItemView{
    @BindString(R.string.s_buscar_productos) protected String titlebar;
    @Bind(R.id.ll_root) LinearLayout ll_root;
    @Bind(R.id.toolbar) protected Toolbar toolbar;
    @Bind(R.id.rv_items) protected RecyclerView rv_items;
    @Bind(R.id.txt_item_name) protected EditText txt_item_name;
    @Bind(R.id.empty_view) protected TextView txtEmpty_view;

    private MenuItem txtNumProducts;

    LayoutInflater inflater;
    private Context context;
    private OnSearchItemListener presenter;
    private ItemsAdapter oAdapter;
    public SearchItemPresenter searchItemPresenter;

    public static final String ITEM_ID_LIST = "ITEM_ID_LIST";
    public static final String ITEM_TO_ORDER = "ITEM_TO_ORDER";

    public static SearchItemFragment instance(){
        return new SearchItemFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_search_item, container, false);
        ButterKnife.bind(this, rootView);
        setToolbar(toolbar, titlebar);
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setHasOptionsMenu(true);

        this.inflater = inflater;
        this.context = getContext();

        rv_items.setHasFixedSize(true);
        rv_items.setLayoutManager(new LinearLayoutManager(context));

        searchItemPresenter = new SearchItemPresenter();
        searchItemPresenter.addView(this);

        return rootView;
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        if (context instanceof OnSearchItemListener) {
            presenter = (OnSearchItemListener) context;
        } else {
            throw new ClassCastException("debe implementar On?Listener");
        }
    }

    @Override
    public void onStart(){
        super.onStart();
        HashSet<Long> item_ids = (HashSet<Long>) activity.getIntent().getSerializableExtra(ITEM_ID_LIST);
        if(item_ids == null)item_ids = new HashSet<>();
        searchItemPresenter.setIdsInOrder(item_ids);
    }

    @OnClick(R.id.btn_search)
    public void searchtems(){
        String item_name = txt_item_name.getText().toString();
        searchItemPresenter.searchItem(item_name);

        View view = getAppActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @OnClick(R.id.btn_clear)
    public void cleartems(){
        txt_item_name.setText("");
        searchItemPresenter.clearSearch();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_item_search, menu);
        txtNumProducts = menu.findItem(R.id.action_add_item);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case android.R.id.home:presenter.onBackPressed();break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void updateTotalItemsSelected(int total) {
        String title = String.valueOf(total);
        if(txtNumProducts != null)
        txtNumProducts.setTitle(title);
    }

    @Override
    public void showProductList(final ArrayList<ItemProductE> items) {
        if(items.size() == 0){
            txtEmpty_view.setVisibility(View.VISIBLE);
            rv_items.setVisibility(View.GONE);
        } else{
            txtEmpty_view.setVisibility(View.GONE);
            rv_items.setVisibility(View.VISIBLE);
        }


        oAdapter = new ItemsAdapter(getContext(), items, new ItemsAdapter.ItemsListener() {
            @Override
            public void onCheckItemClick(int pos) {
                showMessage(getString(R.string.nuevo_producto_agregado));
                searchItemPresenter.addItemInOrder(items.get(pos));
            }
            @Override
            public void onUnCheckItemClick(int pos) {
                searchItemPresenter.removeItemInOrder(items.get(pos));
            }

            @Override
            public boolean isItemSelected(int pos) {
                return searchItemPresenter.isItemIdInOrder(items.get(pos).getId());
            }
        });
        rv_items.setAdapter(oAdapter);
    }

    @Override
    public void showMessage(String message) {
        showMessage(ll_root,message);
    }

    public HashSet<Long> getItemIds() {
        return searchItemPresenter.getIdsInOrder();
    }

    public HashMap<Long, ItemProductE> getItemToOrder() {
        return searchItemPresenter.getItemsInOrderMap();
    }

    public interface OnSearchItemListener{
        void onBackPressed();
    }
}