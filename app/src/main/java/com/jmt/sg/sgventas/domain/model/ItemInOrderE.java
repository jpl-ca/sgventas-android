package com.jmt.sg.sgventas.domain.model;

import java.io.Serializable;

import io.realm.annotations.PrimaryKey;

/**
 * Created by jmtech on 5/16/16.
 */
public class ItemInOrderE implements Serializable {
    @PrimaryKey
    private long id;

    private long product_id;

    private String name;

    private int quantity;

    private double price;

    private double total;

    private String symbol;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getProduct_id() {
        return product_id;
    }

    public void setProduct_id(long product_id) {
        this.product_id = product_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getTotal() {
        return price * quantity;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getSymbol() {
        if(symbol == null)return "S/.";
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}