package com.jmt.sg.sgventas.data.mapper;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jmt.sg.sgventas.data.model.ItemInOrderEntity;
import com.jmt.sg.sgventas.data.model.ItemProductResponse;
import com.jmt.sg.sgventas.data.model.OrderToSendEntity;
import com.jmt.sg.sgventas.domain.model.ItemProductE;
import com.jmt.sg.sgventas.domain.model.OrderToSendE;

import java.util.ArrayList;

/**
 * Created by jmtech on 5/16/16.
 */
public class SendOrderDataMapper {
    Gson gson;

    public SendOrderDataMapper(Gson gson){
        this.gson = gson;
    }

    public OrderToSendEntity transformOrderToSend(OrderToSendE objResponse){
        String str = gson.toJson(objResponse);
        OrderToSendEntity objTransform = gson.fromJson(str,OrderToSendEntity.class);
        return objTransform;
    }
}