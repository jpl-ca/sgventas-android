package com.jmt.sg.sgventas.tracking.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;

import com.jmt.sg.sgventas.domain.model.LocationHistoryE;
import com.jmt.sg.sgventas.presentation.presenter.RouteGeolocationPresenter;
import com.jmt.sg.sgventas.presentation.utils.Constants;
import com.jmt.sg.sgventas.presentation.view.RouteGeolocationView;
import com.jmt.sg.sgventas.tracking.GpsConstant;
import com.jmt.sg.sgventas.tracking.helper.LocationHelper;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by jmtech on 6/1/16.
 */
public class GeoLocationReceiver extends BroadcastReceiver implements RouteGeolocationView{
    private final String TAG = "GeoLocationReceiver";
    private final int MAX_DISTANCE_ALLOWED = 50;
    private Context ctx;
    private RouteGeolocationPresenter routeGeolocationPresenter;

    public GeoLocationReceiver(){
        routeGeolocationPresenter = new RouteGeolocationPresenter();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if(GpsConstant.gpsLocation==null)
            return;

        ctx = context;
        routeGeolocationPresenter.addView(this);
        sendLocations();
    }

    public void sendLocations(){
        System.out.println("</LocGPS*************************************");

        Location gpsLocation = GpsConstant.gpsLocation;
        routeGeolocationPresenter.registerRouteGeolocation(gpsLocation.getLatitude(), gpsLocation.getLongitude());

        System.out.println("*************************************LocGPS/>");
    }

    @Override
    public Context getContext() {
        return ctx;
    }
}