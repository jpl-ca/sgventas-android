package com.jmt.sg.sgventas.data.repository;

import com.jmt.sg.sgventas.data.datasource.OrderToSendDataSourceFactory;
import com.jmt.sg.sgventas.domain.interactor.OrderToSendInteractor;
import com.jmt.sg.sgventas.domain.model.OrderToSendE;
import com.jmt.sg.sgventas.domain.repository.OrderToSendRepository;

/**
 * Created by jmtech on 5/13/16.
 */
public class OrderToSendDataRepository implements OrderToSendRepository {

    private static final String TAG = "LoginDataRepository";
    private final OrderToSendDataSourceFactory orderToSendDataSourceFactory;

    public OrderToSendDataRepository(OrderToSendDataSourceFactory orderToSendDataSourceFactory) {
        this.orderToSendDataSourceFactory = orderToSendDataSourceFactory;
    }

    @Override
    public void saveOrderToSend(long visit_id, OrderToSendE orderToSend, OrderToSendInteractor.Callback oCallback) {

    }

    @Override
    public void getOrderToSend(long visit_id, OrderToSendInteractor.Callback oCallback) {

    }

    @Override
    public void removeOrderToSend(long visit_id, OrderToSendInteractor.Callback oCallback) {

    }
}