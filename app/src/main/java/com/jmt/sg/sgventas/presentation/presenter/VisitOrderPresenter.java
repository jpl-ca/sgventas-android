package com.jmt.sg.sgventas.presentation.presenter;

import android.content.Context;
import android.content.Intent;
import android.location.Location;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.jmt.sg.sgventas.R;
import com.jmt.sg.sgventas.data.datasource.ChangeOrderStateDataSourceFactory;
import com.jmt.sg.sgventas.data.datasource.RescheduleVisitDataSourceFactory;
import com.jmt.sg.sgventas.data.datasource.SendOrderDataSourceFactory;
import com.jmt.sg.sgventas.data.mapper.SendOrderDataMapper;
import com.jmt.sg.sgventas.data.repository.ChangeOrderStateDataRepository;
import com.jmt.sg.sgventas.data.repository.RescheduleVisitDataRepository;
import com.jmt.sg.sgventas.data.repository.SendOrderDataRepository;
import com.jmt.sg.sgventas.domain.interactor.ChangeOrderStateInteractor;
import com.jmt.sg.sgventas.domain.interactor.RescheduleVisitInteractor;
import com.jmt.sg.sgventas.domain.interactor.SendOrderInteractor;
import com.jmt.sg.sgventas.domain.model.ItemInOrderE;
import com.jmt.sg.sgventas.domain.model.ItemProductE;
import com.jmt.sg.sgventas.domain.model.OrderToSendE;
import com.jmt.sg.sgventas.domain.model.ProductOrderE;
import com.jmt.sg.sgventas.domain.model.RouteVisitE;
import com.jmt.sg.sgventas.domain.repository.RescheduleVisitRepository;
import com.jmt.sg.sgventas.domain.repository.SendOrderRepository;
import com.jmt.sg.sgventas.presentation.ui.fragment.SearchItemFragment;
import com.jmt.sg.sgventas.presentation.ui.fragment.VisitOrderFragment;
import com.jmt.sg.sgventas.presentation.utils.Constants;
import com.jmt.sg.sgventas.presentation.view.VisitOrderView;
import com.jmt.sg.sgventas.tracking.invoker.GpsInvoker;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 * Created by jmtech on 5/12/16.
 */
public class VisitOrderPresenter implements Presenter<VisitOrderView>, SendOrderInteractor.Callback{
    private Context context;
    private VisitOrderView visitOrderView;

    private final RouteVisitE routeVisit;
    private GpsInvoker gpsInvoker;

    private ArrayList<ItemInOrderE> itemInOrderList;
    private HashMap<Long,ItemInOrderE> itemInOrderMap;
    private OrderToSendE orderToSend;
    private HashSet<Long> idsInOrder;

    private SendOrderInteractor sendOrderInteractor;
    private RescheduleVisitInteractor rescheduleVisitInteractor;
    private ChangeOrderStateInteractor changeOrderStateInteractor;

    public VisitOrderPresenter(){
        orderToSend = new OrderToSendE();
        itemInOrderList = new ArrayList<>();
        idsInOrder = new HashSet<>();
        itemInOrderMap = new HashMap<>();
        routeVisit = new RouteVisitE();
    }

    public VisitOrderPresenter(Intent intent){
        orderToSend = new OrderToSendE();
        itemInOrderList = new ArrayList<>();
        idsInOrder = new HashSet<>();
        itemInOrderMap = new HashMap<>();

        routeVisit = (RouteVisitE) intent.getSerializableExtra(VisitOrderFragment.ROUTE_VISIT_DATA);

        if(routeVisit.getProduct_order() == null){
            ProductOrderE prod = new ProductOrderE();
            prod.setProduct_order_items(new ArrayList<ItemInOrderE>());
            routeVisit.setProduct_order(prod);
        }
    }

    public void reloadItemsToOrder(Intent data) {
        Serializable items_id_set = data.getSerializableExtra(SearchItemFragment.ITEM_ID_LIST);

        HashSet<Long> items_id = (HashSet<Long>) items_id_set;
        if(items_id == null) items_id = new HashSet<>();

        reloadItemsToOrder(items_id);
    }

    public void reloadItemsToOrderSelected(Intent data) {
        Serializable items_selected = data.getSerializableExtra(SearchItemFragment.ITEM_TO_ORDER);

        HashMap<Long, ItemProductE> item_product_selected = (HashMap<Long, ItemProductE>) items_selected;
        if(item_product_selected == null) item_product_selected = new HashMap<>();

        reloadItemsToOrderSelected(item_product_selected);
    }

    @Override
    public void addView(VisitOrderView view) {
        visitOrderView = view;
        context = view.getContext();

        SendOrderRepository sendOrderRepository = new SendOrderDataRepository(new SendOrderDataSourceFactory(context), new SendOrderDataMapper(new Gson()));
        sendOrderInteractor = new SendOrderInteractor(sendOrderRepository);

        RescheduleVisitRepository rescheduleVisitRepository = new RescheduleVisitDataRepository(new RescheduleVisitDataSourceFactory(context));
        rescheduleVisitInteractor = new RescheduleVisitInteractor(rescheduleVisitRepository);

        ChangeOrderStateDataRepository changeOrderStateDataRepository = new ChangeOrderStateDataRepository(new ChangeOrderStateDataSourceFactory(context));
        changeOrderStateInteractor = new ChangeOrderStateInteractor(changeOrderStateDataRepository);

        gpsInvoker = new GpsInvoker(context);

        visitOrderView.settingData(routeVisit);

        visitOrderView.setVisitPointState(routeVisit.getVisit_state_id());

        visitOrderView.setOrderState(routeVisit.getProduct_order().getState_id());
        if(routeVisit.getVisit_state_id() != Constants.VISIT_STATE.ReScheduling && routeVisit.getProduct_order().getState_id() != Constants.ORDER_STATE.Anulled) {
            setInitialValuesToOrder(routeVisit.getProduct_order().getProduct_order_items());
        }

    }


    private void setInitialValuesToOrder(ArrayList<ItemInOrderE> routeVisit) {
        for (ItemInOrderE itemorder: routeVisit) {
            itemInOrderList.add(itemorder);
            idsInOrder.add(itemorder.getProduct_id());
            itemInOrderMap.put(itemorder.getProduct_id(),itemorder);
        }
        showItemsInOrder();
    }

    @Override
    public void removeView() {
        visitOrderView = null;
    }

    public RouteVisitE getRouteVisit() {
        return routeVisit;
    }

    public HashSet<Long> getItemIds() {
        return idsInOrder;
    }


    private void resetItemsInOrder() {
        orderToSend = new OrderToSendE();
        itemInOrderList = new ArrayList<>();
        itemInOrderMap = new HashMap<>();

        reloadItemsToOrder(new HashSet<Long>());
        reloadItemsToOrderSelected(new HashMap<Long, ItemProductE>());
    }

    /** Recargar los items seleccionados, que llegan de la actividad de busqueda de productos */
    public void reloadItemsToOrder(HashSet<Long> items_id) {
        idsInOrder = items_id;
    }

    /** Recargar los datos, que llegan de la actividad de busqueda de productos */
    public void reloadItemsToOrderSelected(HashMap<Long, ItemProductE> item_product_selected) {
        for (long itemid: idsInOrder) {
            int cant = 1;
            ItemInOrderE itemInOrder;
            ItemProductE itemProduct = item_product_selected.get(itemid);
            /** Evaluar si el item actual ya existe en la orden*/
            if(itemInOrderMap.containsKey(itemid)){
                itemInOrder = itemInOrderMap.get(itemid);
                cant = itemInOrder.getQuantity();

                itemInOrder.setQuantity(cant);
            }else{
                itemInOrder = new ItemInOrderE();/** Inicializar nueva orden */
                itemInOrder.setProduct_id(itemProduct.getId());
                itemInOrder.setName(itemProduct.getName());
                itemInOrder.setSymbol(itemProduct.getCurrency_symbol());

                itemInOrder.setQuantity(cant);
                itemInOrder.setPrice(itemProduct.getPrice());
            }
            itemInOrderMap.put(itemid,itemInOrder);
        }

        /** Eliminar los que fueron desselecionados de la busqueda */
        for (long ids: idsInOrder) {
            if(!itemInOrderMap.containsKey(ids))itemInOrderMap.remove(ids);
        }


        /** Preparar nueva lista de Productos para visualizar */
        itemInOrderList = new ArrayList<>();
        for (long itemid: idsInOrder) {
            itemInOrderList.add(itemInOrderMap.get(itemid));
        }

        showItemsInOrder();
    }

    /** Remove item */
    public void removeItemInOrder(int pos) {
        long id = itemInOrderList.get(pos).getProduct_id();
        itemInOrderMap.remove(id);
        idsInOrder.remove(id);
        itemInOrderList.remove(pos);

        showItemsInOrder();
    }

    /** Update Ammount */
    public void updateItemInOrder(int pos, int ammount) {
        double tot = itemInOrderList.get(pos).getPrice() * ammount;
        itemInOrderList.get(pos).setQuantity(ammount);
        itemInOrderList.get(pos).setTotal(tot);

        showItemsInOrder();
    }

    /** Enviar los datos para mostrar en el RecyclerView */
    public void showItemsInOrder() {
        visitOrderView.showItemsInOrder(itemInOrderList);
        String symbol = "";
        double total_sum = 0;
        for (ItemInOrderE itm:itemInOrderList) {
            total_sum += itm.getTotal();
            if(itm.getSymbol().length()>0)symbol = itm.getSymbol();
        }
        System.out.println("SZ IS:"+itemInOrderList.size());
        System.out.println("SUM:"+total_sum);
        visitOrderView.setTotalValue(symbol, total_sum);
    }




    /** Code to call interactor... Send Products Order */

    public void sendCurrentOrder(){
        if(itemInOrderList.size()==0){
            visitOrderView.showMessage(context.getString(R.string.e_la_lista_de_productos_esta_vacio));
            return;
        }

        orderToSend.setDescription("Enviando orden desde SGVentas");
        orderToSend.setProductOrderItems(itemInOrderList);
        sendOrderInteractor.sendOrder(routeVisit.getId(),orderToSend,this);
    }

    @Override
    public void onSendOrderSuccess() {
        visitOrderView.showMessage(context.getString(R.string.pedido_enviado_correctamente));
        visitOrderView.orderListSent();

        routeVisit.getProduct_order().setState_id(Constants.ORDER_STATE.Done);
        visitOrderView.setOrderState(Constants.ORDER_STATE.Pending);

        visitOrderView.onStateChangedToRefresh();
    }

    @Override
    public void onSendOrderError(String message) {
        visitOrderView.showMessage(message);
    }


    public int getItemInOrderSize() {
        return itemInOrderList.size();
    }


    public void rescheduleVisitPoint(String comment) {
        final long visit_point_id = routeVisit.getId();

        rescheduleVisitInteractor.rescheduleVisitPoint(visit_point_id, comment, new RescheduleVisitInteractor.Callback() {
            @Override
            public void onRescheduleSuccess() {
                routeVisit.setVisit_state_id(Constants.VISIT_STATE.ReScheduling);

                visitOrderView.setVisitPointState(Constants.VISIT_STATE.ReScheduling);

                resetItemsInOrder();

                visitOrderView.onStateChangedToRefresh();
            }
            @Override
            public void onRescheduleError(String message) {
                visitOrderView.showMessage(message);
            }
        });
    }

    public void confirmOrder() {
        final long visit_point_id = routeVisit.getId();

        visitOrderView.showMessage(context.getString(R.string.s_orden_pedido_confirmado));

        changeOrderStateInteractor.confirmOrder(visit_point_id, new ChangeOrderStateInteractor.CallbackConfirm() {
            @Override
            public void onConfirmSuccess() {
                routeVisit.getProduct_order().setState_id(Constants.ORDER_STATE.Confirmed);
                visitOrderView.setOrderState(Constants.ORDER_STATE.Confirmed);
                showItemsInOrder();

                visitOrderView.onStateChangedToRefresh();
            }
            @Override
            public void onConfirmError(String message) {
            }
        });
    }

    public void cancelOrder() {
        final long visit_point_id = routeVisit.getId();

        visitOrderView.showMessage(context.getString(R.string.s_orden_pedido_anulado));

        changeOrderStateInteractor.cancelOrder(visit_point_id, new ChangeOrderStateInteractor.CallbackCancel() {
            @Override
            public void onCancelSuccess() {
                routeVisit.getProduct_order().setState_id(Constants.ORDER_STATE.Anulled);
                visitOrderView.setOrderState(Constants.ORDER_STATE.Anulled);
                resetItemsInOrder();

                visitOrderView.onStateChangedToRefresh();
            }
            @Override
            public void onCancelError(String message) {
                visitOrderView.showMessage(message);
            }
        });
    }

    public void getMyLocation(final onGetMyLocation callback) {
        gpsInvoker.startGpsData();
        gpsInvoker.startGpsListener(new GpsInvoker.LocationCallback() {
            @Override
            public void updateLocation(Location location) {
                callback.setMyLocation(new LatLng(location.getLatitude(),location.getLongitude()));
                gpsInvoker.stopGpsListener();
            }
        });
    }

    public interface onGetMyLocation{
        void setMyLocation(LatLng ll);
    }
}