package com.jmt.sg.sgventas.data.datasource.preferences;

import android.content.Context;

import com.jmt.sg.sgventas.data.datasource.ValidateOrganizationDataSource;
import com.jmt.sg.sgventas.domain.repository.RepositoryCallback;

/**
 * Created by jmtech on 5/13/16.
 */
public class PreferencesOrganizationDataSource implements ValidateOrganizationDataSource {

    private final Context context;
    public PreferencesOrganizationDataSource(Context context) {
        this.context= context;
    }

    @Override
    public void validateOrganization(String organization_name, RepositoryCallback repositoryCallback) {
    }
}