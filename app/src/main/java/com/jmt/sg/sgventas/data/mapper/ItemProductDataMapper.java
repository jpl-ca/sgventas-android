package com.jmt.sg.sgventas.data.mapper;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jmt.sg.sgventas.data.model.ItemProductResponse;
import com.jmt.sg.sgventas.domain.model.ItemProductE;

import java.util.ArrayList;

/**
 * Created by jmtech on 5/16/16.
 */
public class ItemProductDataMapper {
    Gson gson;

    public ItemProductDataMapper(Gson gson){
        this.gson = gson;
    }

    public ItemProductE transformItemProduct(ItemProductResponse objResponse){
        String str = gson.toJson(objResponse);
        ItemProductE objTransform = gson.fromJson(str,ItemProductE.class);
        return objTransform;
    }
    public ArrayList<ItemProductE> transformItemProduct(ArrayList<ItemProductResponse> objResponse){
        String str = gson.toJson(objResponse);
        ArrayList<ItemProductE> objTransform = gson.fromJson(str, new TypeToken<ArrayList<ItemProductE>>(){}.getType());
        return objTransform;
    }
}