package com.jmt.sg.sgventas.presentation.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.jmt.sg.sgventas.R;
import com.jmt.sg.sgventas.domain.model.RouteVisitE;
import com.jmt.sg.sgventas.presentation.ui.fragment.SearchItemFragment;
import com.jmt.sg.sgventas.presentation.ui.fragment.VisitOrderFragment;
import com.jmt.sg.sgventas.presentation.utils.Constants;

import java.util.HashSet;

/**
 * Created by JMTech-Android on 29/12/2015.
 */
public class VisitOrderActivity extends AppCompatActivity implements VisitOrderFragment.OnVisitOrderListener{

    public static final int CODE_RESULT = 101;

    private final VisitOrderFragment visitOrderFragment = VisitOrderFragment.instance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visit_order);
        showVisitOrderFragment();
    }

    private void showVisitOrderFragment() {
        getSupportFragmentManager().beginTransaction().replace(R.id.container, visitOrderFragment).commit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == VisitOrderActivity.CODE_RESULT) {
            if(resultCode == RESULT_OK){
                visitOrderFragment.updateItemList(data);
                visitOrderFragment.updateItemToOrderSelected(data);
            }
            if (resultCode == RESULT_CANCELED) {
            }
        }
    }

    @Override
    public void sendResultSuccess(RouteVisitE routeVisit) {
        Intent it = new Intent();
        it.putExtra(Constants.VISIT_STATE.VisitState,routeVisit);
        setResult(RESULT_OK,it);
    }

    @Override
    public void loadItemsActivity(HashSet<Long> item_ids) {
        Intent it = new Intent(this, SearchItemActivity.class);
        it.putExtra(SearchItemFragment.ITEM_ID_LIST,item_ids);
        startActivityForResult(it,SearchItemActivity.CODE_RESULT);
    }
}