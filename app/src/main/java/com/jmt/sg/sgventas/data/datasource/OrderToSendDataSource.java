package com.jmt.sg.sgventas.data.datasource;

import com.jmt.sg.sgventas.data.model.OrderToSendEntity;
import com.jmt.sg.sgventas.domain.model.AgentTrackeable;
import com.jmt.sg.sgventas.domain.repository.RepositoryCallback;

import rx.Observable;

/**
 * Created by jmtech on 5/13/16.
 */
public interface OrderToSendDataSource {
    void saveOrderToSend(long visit_id, OrderToSendEntity user, RepositoryCallback repositoryCallback);

    void getOrderToSend(long visit_id, RepositoryCallback repositoryCallback);

    void removeOrderToSend(long visit_id, RepositoryCallback repositoryCallback);

}