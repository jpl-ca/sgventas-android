package com.jmt.sg.sgventas.presentation.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.jmt.sg.sgventas.R;
import com.jmt.sg.sgventas.domain.model.RouteVisitE;
import com.jmt.sg.sgventas.presentation.ui.fragment.SearchItemFragment;
import com.jmt.sg.sgventas.presentation.ui.fragment.TimelineFragment;
import com.jmt.sg.sgventas.presentation.utils.Constants;

import butterknife.ButterKnife;

/**
 * Created by JMTech-Android on 29/12/2015.
 */
public class SearchItemActivity extends AppCompatActivity implements SearchItemFragment.OnSearchItemListener{

    public static final int CODE_RESULT = 101;

    private final SearchItemFragment searchItemFragment = SearchItemFragment.instance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_item);
        showFragment();
    }

    private void showFragment() {
        getSupportFragmentManager().beginTransaction().replace(R.id.container, searchItemFragment).commit();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onBackPressed() {
        Intent it = new Intent();
        it.putExtra(SearchItemFragment.ITEM_ID_LIST,searchItemFragment.getItemIds());
        it.putExtra(SearchItemFragment.ITEM_TO_ORDER,searchItemFragment.getItemToOrder());
        setResult(RESULT_OK,it);
        super.onBackPressed();
    }
}