package com.jmt.sg.sgventas.domain.repository;

import com.jmt.sg.sgventas.domain.interactor.LoginInteractor;

/**
 * Created by jmtech on 5/12/16.
 */
public interface LoginRepository {
    void loginAgent(String identification_code, String password, final LoginInteractor.Callback loginIteractorCallback);
}