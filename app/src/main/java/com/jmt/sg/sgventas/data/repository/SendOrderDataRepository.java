package com.jmt.sg.sgventas.data.repository;

import com.jmt.sg.sgventas.data.datasource.SendOrderDataSource;
import com.jmt.sg.sgventas.data.datasource.SendOrderDataSourceFactory;
import com.jmt.sg.sgventas.data.mapper.SendOrderDataMapper;
import com.jmt.sg.sgventas.data.mapper.TrackeableDataMapper;
import com.jmt.sg.sgventas.data.model.DataSourceFactory;
import com.jmt.sg.sgventas.data.model.OrderToSendEntity;
import com.jmt.sg.sgventas.domain.interactor.SendOrderInteractor;
import com.jmt.sg.sgventas.domain.model.OrderToSendE;
import com.jmt.sg.sgventas.domain.repository.RepositoryCallback;
import com.jmt.sg.sgventas.domain.repository.SendOrderRepository;

/**
 * Created by jmtech on 5/13/16.
 */
public class SendOrderDataRepository implements SendOrderRepository {

    private static final String TAG = "LoginDataRepository";
    private final SendOrderDataSourceFactory sendOrderDataSourceFactory;
    private final SendOrderDataMapper sendOrderDataMapper;

    public SendOrderDataRepository(SendOrderDataSourceFactory sendOrderDataSourceFactory, SendOrderDataMapper sendOrderDataMapper) {
        this.sendOrderDataSourceFactory = sendOrderDataSourceFactory;
        this.sendOrderDataMapper = sendOrderDataMapper;
    }

    @Override
    public void sendOrder(long visitPointId, OrderToSendE orderToSendE, final SendOrderInteractor.Callback sCallback) {

        OrderToSendEntity orderToSendEntity = sendOrderDataMapper.transformOrderToSend(orderToSendE);

        SendOrderDataSource sendOrderDataSource = sendOrderDataSourceFactory.create(DataSourceFactory.CLOUD);
        sendOrderDataSource.sendOrder(visitPointId, orderToSendEntity, new RepositoryCallback() {
            @Override
            public void onSuccess(Object object) {
                sCallback.onSendOrderSuccess();
            }
            @Override
            public void onError(Object object) {
                String message = "";
                if(object!=null)
                    message= object.toString();
                System.out.println(TAG+"->"+message);
                sCallback.onSendOrderError(message);
            }
        });
    }
}