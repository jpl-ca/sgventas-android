package com.jmt.sg.sgventas.data.model;

import com.jmt.sg.sgventas.data.mapper.BaseResponse;

/**
 * Created by jmtech on 5/16/16.
 */
public class LatLngUpdateRequest extends BaseResponse {
    double lat;
    double lng;
    public LatLngUpdateRequest(double Lat, double Lng){
        this.lat = Lat;
        this.lng = Lng;
    }
}