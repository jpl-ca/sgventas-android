package com.jmt.sg.sgventas.data.repository;

import com.jmt.sg.sgventas.data.datasource.SearchItemsDataSource;
import com.jmt.sg.sgventas.data.datasource.SearchItemsDataSourceFactory;
import com.jmt.sg.sgventas.data.mapper.ItemProductDataMapper;
import com.jmt.sg.sgventas.data.mapper.TrackeableDataMapper;
import com.jmt.sg.sgventas.data.model.DataSourceFactory;
import com.jmt.sg.sgventas.data.model.LoginTrackeableResponse;
import com.jmt.sg.sgventas.data.model.SearchItemResponse;
import com.jmt.sg.sgventas.domain.interactor.SearchItemsInteractor;
import com.jmt.sg.sgventas.domain.model.AgentTrackeable;
import com.jmt.sg.sgventas.domain.model.ItemProductE;
import com.jmt.sg.sgventas.domain.repository.RepositoryCallback;
import com.jmt.sg.sgventas.domain.repository.SearchItemsRepository;

import java.util.ArrayList;

/**
 * Created by jmtech on 5/13/16.
 */
public class SearchItemsDataRepository implements SearchItemsRepository {

    private static final String TAG = "LoginDataRepository";
    private final SearchItemsDataSourceFactory searchItemsDataSourceFactory;
    private final ItemProductDataMapper itemProductDataMapper;

    public SearchItemsDataRepository(SearchItemsDataSourceFactory searchItemsDataSourceFactory, ItemProductDataMapper itemProductDataMapper) {
        this.searchItemsDataSourceFactory = searchItemsDataSourceFactory;
        this.itemProductDataMapper = itemProductDataMapper;
    }

    @Override
    public void searchItems(String name, final SearchItemsInteractor.Callback callback) {
        SearchItemsDataSource searchItemsDataSource = searchItemsDataSourceFactory.create(DataSourceFactory.CLOUD);
        searchItemsDataSource.searchItems(name, new RepositoryCallback() {
            @Override
            public void onSuccess(Object object) {
                SearchItemResponse siData = (SearchItemResponse) object;
                ArrayList<ItemProductE> itemProductList = itemProductDataMapper.transformItemProduct(siData.getData());
                callback.onSearchItemsSuccess(itemProductList);
            }
            @Override
            public void onError(Object object) {
                String message = "";
                if(object!=null)
                    message= object.toString();
                System.out.println(TAG+"->"+message);
                callback.onSearchItemsError(message);
            }
        });
    }
}