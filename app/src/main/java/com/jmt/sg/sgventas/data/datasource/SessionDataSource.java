package com.jmt.sg.sgventas.data.datasource;

import com.jmt.sg.sgventas.domain.repository.RepositoryCallback;

/**
 * Created by jmtech on 5/13/16.
 */
public interface SessionDataSource {
    void checkSession(RepositoryCallback repositoryCallback);
    void closeSession(RepositoryCallback repositoryCallback);
}