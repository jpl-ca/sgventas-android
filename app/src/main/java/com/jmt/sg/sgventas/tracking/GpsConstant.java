package com.jmt.sg.sgventas.tracking;

import android.location.Location;
import android.support.annotation.Nullable;

import com.jmt.sg.sgventas.domain.model.LocationHistoryE;

/**
 * Created by jmtech on 5/24/16.
 */
public class GpsConstant {
    public static String RECEIVE_GPS = "com.jmt.sg.sgtventas.intent.action.RECEIVE_LOCATION";
    public static Location gpsLocation;
}