package com.jmt.sg.sgventas.domain.model;

import java.io.Serializable;
import java.util.ArrayList;

import io.realm.annotations.PrimaryKey;

/**
 * Created by jmtech on 5/16/16.
 */
public class OrderToSendE implements Serializable{
    @PrimaryKey
    private long id;

    private String description;

    private long tasks_visit_point_id;

    private String status;

    private ArrayList<ItemInOrderE> productOrderItems;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getTasks_visit_point_id() {
        return tasks_visit_point_id;
    }

    public void setTasks_visit_point_id(long tasks_visit_point_id) {
        this.tasks_visit_point_id = tasks_visit_point_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<ItemInOrderE> getProductOrderItems() {
        return productOrderItems;
    }

    public void setProductOrderItems(ArrayList<ItemInOrderE> productOrderItems) {
        this.productOrderItems = productOrderItems;
    }
}