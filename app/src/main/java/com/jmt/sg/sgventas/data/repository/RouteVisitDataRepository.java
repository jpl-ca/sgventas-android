package com.jmt.sg.sgventas.data.repository;

import com.jmt.sg.sgventas.R;
import com.jmt.sg.sgventas.data.datasource.RouteVisitDataSource;
import com.jmt.sg.sgventas.data.datasource.RouteVisitDataSourceFactory;
import com.jmt.sg.sgventas.data.mapper.RouteVisitDataMapper;
import com.jmt.sg.sgventas.data.model.DataSourceFactory;
import com.jmt.sg.sgventas.data.model.RouteResponse;
import com.jmt.sg.sgventas.data.model.RouteVisitResponse;
import com.jmt.sg.sgventas.domain.interactor.RouteVisitInteractor;
import com.jmt.sg.sgventas.domain.model.RouteVisitE;
import com.jmt.sg.sgventas.domain.repository.RepositoryCallback;
import com.jmt.sg.sgventas.domain.repository.RouteVisitRepository;

import java.util.ArrayList;

/**
 * Created by jmtech on 5/13/16.
 */
public class RouteVisitDataRepository implements RouteVisitRepository {
    private final RouteVisitDataSourceFactory routeVisitDataSourceFactory;
    private final RouteVisitDataMapper visitDataMapper;

    public RouteVisitDataRepository(RouteVisitDataSourceFactory routeVisitDataSourceFactory, RouteVisitDataMapper visitDataMapper) {
        this.routeVisitDataSourceFactory = routeVisitDataSourceFactory;
        this.visitDataMapper = visitDataMapper;
    }

    @Override
    public void getRouteVisit(final RouteVisitInteractor.Callback rouInteractorCallback) {
        RouteVisitDataSource routeVisitDataSource = routeVisitDataSourceFactory.create(DataSourceFactory.CLOUD);
        routeVisitDataSource.getRouteVisitData(new RepositoryCallback() {
            @Override
            public void onSuccess(Object object) {
                RouteResponse routeR = (RouteResponse)object;
                ArrayList<RouteVisitResponse> routeVisitsR = routeR.getData();
                ArrayList<RouteVisitE> routeVisits = visitDataMapper.transformRoute(routeVisitsR);
                rouInteractorCallback.onGetRouteSuccess(routeVisits);
            }
            @Override
            public void onError(Object object) {
                String message = "";
                if(object!=null){
                    message= object.toString();
                }
                rouInteractorCallback.onGetRouteError(message);
            }
        });
    }
}