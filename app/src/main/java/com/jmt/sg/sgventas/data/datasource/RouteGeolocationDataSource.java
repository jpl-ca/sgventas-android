package com.jmt.sg.sgventas.data.datasource;

import com.jmt.sg.sgventas.data.model.GeolocationUpdateRequest;
import com.jmt.sg.sgventas.domain.repository.RepositoryCallback;

/**
 * Created by jmtech on 5/13/16.
 */
public interface RouteGeolocationDataSource {
    void storeGeolocation(double lat, double lng, RepositoryCallback repositoryCallback);
    void storeGeolocation(GeolocationUpdateRequest geolocationUpdateRequest, RepositoryCallback repositoryCallback);
}