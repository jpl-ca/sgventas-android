package com.jmt.sg.sgventas.domain.model;

import java.io.Serializable;

/**
 * Created by jmtech on 5/16/16.
 */
public class LocationHistoryE implements Serializable {
    private double lat;
    private double lng;
    private float bearing;
    private float speed;
    private double distance;

    public LocationHistoryE(double lat, double lng) {
        this.lat = lat;
        this.lng = lng;
    }
    public LocationHistoryE(double lat, double lng, float bearing, float speed) {
        this.lat = lat;
        this.lng = lng;
        this.bearing = bearing;
        this.speed = speed;
    }
    public LocationHistoryE(double lat, double lng, float bearing, float speed,double distance) {
        this.lat = lat;
        this.lng = lng;
        this.bearing = bearing;
        this.speed = speed;
        this.distance = distance;
    }
    public LocationHistoryE(double lat, double lng, double distance) {
        this.lat = lat;
        this.lng = lng;
        this.distance = distance;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public float getBearing() {
        return bearing;
    }

    public void setBearing(float bearing) {
        this.bearing = bearing;
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    @Override
    public String toString() {
        return "</"+lat + "," + lng + " | "+String.format("%.5f",distance) + "/>   ";
//        return "</"+lat + "," + lng + " | (" + bearing + ")("+speed+") | "+String.format("%.4f",distance) + "/>   ";
    }
}