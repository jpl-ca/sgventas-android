package com.jmt.sg.sgventas.data.repository;

import com.jmt.sg.sgventas.data.datasource.TrackeableDataSource;
import com.jmt.sg.sgventas.data.datasource.TrackeableDataSourceFactory;
import com.jmt.sg.sgventas.data.mapper.TrackeableDataMapper;
import com.jmt.sg.sgventas.data.model.DataSourceFactory;
import com.jmt.sg.sgventas.domain.interactor.TrackerSaveInteractor;
import com.jmt.sg.sgventas.domain.model.AgentTrackeable;
import com.jmt.sg.sgventas.domain.repository.RepositoryCallback;
import com.jmt.sg.sgventas.domain.repository.TrackerSaveDataRepository;

/**
 * Created by jmtech on 5/13/16.
 */
public class TrackerDataSaveRepository implements TrackerSaveDataRepository {
    private final TrackeableDataSourceFactory trackeableDataSourceFactory;
    private final TrackeableDataMapper trackeableDataMapper;

    public TrackerDataSaveRepository(TrackeableDataSourceFactory trackeableDataSourceFactory, TrackeableDataMapper trackeableDataMapper) {
        this.trackeableDataSourceFactory = trackeableDataSourceFactory;
        this.trackeableDataMapper= trackeableDataMapper;
    }

    @Override
    public void saveTrackerData(AgentTrackeable aTrackeable, final TrackerSaveInteractor.Callback aCallback) {
        TrackeableDataSource trackeableDataSource = trackeableDataSourceFactory.create(DataSourceFactory.DB);
        trackeableDataSource.saveAgent(aTrackeable, new RepositoryCallback() {
            @Override
            public void onSuccess(Object object) {
                aCallback.onTrackerDataSaved();
            }

            @Override
            public void onError(Object object) {
                String mess = "";
                aCallback.onTrackerDataSavedError(mess);
            }
        });
    }
}