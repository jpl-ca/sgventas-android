package com.jmt.sg.sgventas.presentation.ui.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.jmt.sg.sgventas.SGApplication;

import butterknife.ButterKnife;

/**
 * Created by jmtech on 5/12/16.
 */
public class BaseActivity extends AppCompatActivity{

    protected SGApplication sgApp;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sgApp = (SGApplication)this.getApplicationContext();
    }

}