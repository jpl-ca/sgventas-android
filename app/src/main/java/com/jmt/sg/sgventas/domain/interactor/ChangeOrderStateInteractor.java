package com.jmt.sg.sgventas.domain.interactor;

import com.jmt.sg.sgventas.domain.repository.ChangeStateOrderRepository;

/**
 * Created by jmtech on 5/12/16.
 */
public class ChangeOrderStateInteractor {
    private final ChangeStateOrderRepository changeStateOrderRepository;

    public ChangeOrderStateInteractor(ChangeStateOrderRepository changeStateOrderRepository) {
        this.changeStateOrderRepository = changeStateOrderRepository;
    }

    public void confirmOrder(final long visit_point_id,CallbackConfirm callback){
        changeStateOrderRepository.confirmOrder(visit_point_id,callback);
    }

    public void cancelOrder(final long visit_point_id,CallbackCancel callback){
        changeStateOrderRepository.cancelOrder(visit_point_id,callback);
    }

    public interface CallbackConfirm {
        void onConfirmSuccess();
        void onConfirmError(String message);
    }
    public interface CallbackCancel {
        void onCancelSuccess();
        void onCancelError(String message);
    }
}