package com.jmt.sg.sgventas.data.datasource.db.realm;

import com.jmt.sg.sgventas.domain.model.AgentTrackeable;

import rx.Observable;

/**
 * Created by JMTech-Android on 26/01/2016.
 */
public interface AgentDataServiceI {
    Observable<AgentTrackeable> getAgent();
    Observable<Boolean> registerAgent(AgentTrackeable user);
    Observable<Boolean> removeAgent();
}